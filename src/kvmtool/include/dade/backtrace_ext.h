/*                                                                              
 * Project name: DADE - Data Anomaly Detection Engine                           
 * Project managers: José Luis Briz and Darío Suárez                            
 *                   Computer Architecture Group                                
 *                   University of Zaragoza                                     
 * Author: Luis Fueris Martín                                                   
 * Date: 28 feb 2018                                                            
 * File name: backtrace_ext.h                                                   
 */       

#ifndef DADE__BACKTRACE_EXT
#define DADE__BACKTRACE_EXT

#include <linux/types.h>
#include <openssl/md5.h>


#define BUFSIZE     1024
#define ADDR_LEN	16

/* the backtrace is a list (not double linked) of addr */
struct backtrace {
	char 	*addr;
	char	*func_name;
	struct 	list_head list;
};

/* backtrace components */
struct backtrace_container {
	struct backtrace	btr;
	char				*obj_name;
	char				*obj_value;
	char 				hash[MD5_DIGEST_LENGTH * 2 + 1];
};

int backtrace_ext__init(char *filename);
int backtrace_ext__save_stacktrace(struct list_head *result_list, int list_length, char* obj_name, int obj_name_len);

#endif /* DADE__BACKTRACE_EXT */

# Kvmtool Hypervisor

1. [Summary](#Summary)
2. [Guest kernel configuration](#GuestKernelConf)
3. [Usage and debug](#UsageDebug)
4. [References](#References)

## Summary <a name="Summary"></a>

The lightweight type two Hypervisor [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) was initially developed by Pekka Enberg, Cyrill Gorcunov, Asias He, Sasha Levin and Prasad Josh with the help of Avi Kivity and Ingo Molnar. Nowadays the latest commits are made by Jean-Philippe Brucker, Andre Przywara, Dmitry Monakhov and Robin Murphy. This Hypervisor is used to launch *guest* instances on nodes which do not have a lot of resources (for instance a laptop) so it can be defined like a anorexic Hypervisor. Furthermore, it is intended for different purposes:

- Kernel development and debugging of *guest* instances. 
- SMP (*Symmetric Multi-Processing*) applications. 
- Study basic concepts of virtualization. 


It also supports [Real-Time](https://rt.wiki.kernel.org/index.php/Main_Page) patch, a maximun of 254 virtualized CPUs and 32-bit applications in 64-bit kernel . These aspects may be of interest to kernel developers, distributed and parallel systems developers and virtualization engineers. To do this stuff it uses [KVM kernel module](https://elixir.bootlin.com/linux/latest/source/virt/kvm) to virtualize memory, disk, CPUs, interrupts. The [QEMU/KVM](https://www.qemu.org) Hypervisor is located at a higher level than this tool but in a future [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) can be a substitute. 

This tool has been chosen because it launchs quickly and easily *guest* instances. So we can study how Hypervisor works and detect anomalies on internal kernel structures which is the main goal of this bachelor's thesis. 

## Guest kernel configuration <a name="GuestKernelConf"></a>

If you want to launch a *guest* instance, you need to compile the kernel with the following options of the ``.config`` file.

- Defines standard serial port to use in *guest* instance. Ref. *\<ksrc\>/drivers/tty/serial/8250/Kconfig*.

```bash
CONFIG_SERIAL_8250=y
```
- Defines the use of serial port as default system console. This console shows kernel messages. It can change if you specify the following kernel parameter ``console=ttyS1``. Please, if you need more information, refer to manual ``man bootparam``. Ref. *\<ksrc\>/drivers/tty/serial/8250/Kconfig*.

```bash
CONFIG_SERIAL_8250_CONSOLE=y
```

- Defines the code to execute 32-bit application under a 64-bit kernel. Ref. *\<ksrc\>/arch/x86/Kconfig*.

```bash
CONFIG_IA32_EMULATION=y
```

- Defines the main platform of I/O drivers virtualization. This option is selected for any driver that uses ``virtio``. Ref. [linux-kvm.org/page/Virtio](https://www.linux-kvm.org/page/Virtio).

```bash
CONFIG_VIRTIO=y
```

- Defines the usage of *ring buffers*. These buffers are used by ``virtio`` devices. Ref. *\<ksrc\>/drivers/virtio/virtio_ring.c* and [wiki.osdev.org/Virtio](http://wiki.osdev.org/Virtio).

```bash
CONFIG_VIRTIO_RING=y
```

- Defines the virtualization of PCI driver. The hypevisor must have backends drivers of PCI ``virtio``. Ref. *\<ksrc\>/drivers/virtio/Kconfig*.

```bash
CONFIG_VIRTIO_PCI=y
```

- Defines the virtualization of block devices. Ref. *\<ksrc\>/drivers/block/Kconfig*.

```bash
CONFIG_VIRTIO_BLK=y (opción --disk)
```

- Defines the network virtualization that it is used by ``virtio``.

```bash
CONFIG_VIRTIO_NET=y (opción --network)
```

- Defines the use of [9pfs protocol](https://xenbits.xen.org/docs/4.9-testing/misc/9pfs.html) via a network connection. This protocol
can be used as remote filesystem that communicates *guest* and *host* via socket. Ref. *\<ksrc\>/net/9p/Kconfig*.

``` bash
CONFIG_NET_9P=y (opción --virtio-9p)
```

- Defines the network virtualization of [9pfs protocol](https://xenbits.xen.org/docs/4.9-testing/misc/9pfs.html). In this way *host* can define a filesystem for the *guest*. Ref. *\<ksrc\>/net/9p/Kconfig*.

``` bash
CONFIG_NET_9P_VIRTIO=y
```

- Defines the use of [9pfs protocol](https://xenbits.xen.org/docs/4.9-testing/misc/9pfs.html) as one of filesystems. Ref. *\<ksrc\>/fs/9p/Kconfig*.

``` bash
CONFIG_9P_FS=y
```

- Defines ``virtio`` driver which is used to increase and decrease *guest* memory. It is convenient to qualify that a *balloon* driver is a basic [virtualization technique](https://scholar.google.es/scholar?q=balloon+technique+virtualization&hl=es&as_sdt=0&as_vis=1&oi=scholart). Ref. *\<ksrc\>/drivers/virtio/Kconfig*.
 
``` bash
CONFIG_VIRTIO_BALLOON=y (opción --balloon)
```

- Defines the virtualization of ``virtio`` console. It is more faster than a *serial 82590*, as well as consumes fewer resources. An example of the usage is the following device ``/dev/vport<N>p<n>`` where ``N`` defines device number and ``n`` defines the device port. Ref. *\<ksrc\>/drivers/char/Kconfig*.

``` bash
CONFIG_VIRTIO_CONSOLE=y (opción --console virtio)
```

- Defines the virtualization of random numbers by hardware. Ref. *\<ksrc\>/drivers/char/hw_random/Kconfig*.

``` bash
CONFIG_HW_RANDOM_VIRTIO=y (opción --rng)
```

- Defines device drive which uses a buffer for VESA graphic unit 2.0. Ref. *\<ksrc\>/drivers/video/Kconfig*

``` bash
CONFIG_FB_VESA=y (opción --sdl o --vnc)
```

The steps to compile the kernel can be found on Section [kernel binaries](./doc/README.md). 

## Usage and debug <a name="UsageDebug"></a>

Now we are going to show usage and debug of [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README). Firstly you need to download [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) and compile it.

```bash
$ git clone https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git
$ cd kvmtool/
$ make
```

It generates ``lkvm``binary which is the command line interface to launch virtual machines (or *guests* instances). If you want to launch a default *guest*, please execute ``lkvm`` as follows. 

```bash
host $> lkvm run
  # lkvm run -k /boot/vmlinuz-4.4.0-135-arm64 -m 704 -c 8 --name guest-1224
  Info: Loaded kernel to 0x80080000 (15869952 bytes)
  Info: Placing fdt at 0x8fe00000 - 0x8fffffff
  Info: virtio-mmio.devices=0x200@0x10000:36

  Info: virtio-mmio.devices=0x200@0x10200:37

  Info: virtio-mmio.devices=0x200@0x10400:38

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
...
```

It is convenient to note that ``lkvm`` tool choose default *host* kernel from ``/boot``directory. It executes a *guest* with ``704 MB`` of memory, ``8`` vCPUs and call it by the name of ``guest-1224`` (the suffix is process *pid*). If you run it this way and the host kernel is not compiled with the options described above, the virtual machine crashes with a kernel panic because [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) takes default setting and treats the kernel like it is compressed  which it is incorrect. The kernel ``/boot/vmlinuz-4.4.0-135-arm64`` is a decompressed kernel since *host* needs it to boot the real operating system on AArch64. If you need more information about it, please check the kernel [documentation of kernel](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/arm64/booting.txt?id=refs/tags/v4.8-rc4). If the kernel fails, it is recommended to erase the socket ``.lkvm/guest-<pid>.sock`` that is in the user's directory. So please compile the kernel with the options described in [Guest kernel configuration](#GuestKernelConf). 

Now we want to run a virtual machine with following settings:

- Kernel image is on ``/media/user/sdcard/``
- The *guest* name is ``foo``
- Memory ``254 MB``
- vCPUs ``2``


A right execiton can be the succeeding:

```bash
host $> lkvm run -k /media/user/sdcard/Image -m 254 -c 2 --name foo
  # lkvm run -k /media/user/sdcard/Image -m 254 -c 2 --name foo
  Info: Loaded kernel to 0x80080000 (12040672 bytes)
  Info: Placing fdt at 0x8fc00000 - 0x8fdfffff
  Info: virtio-mmio.devices=0x200@0x10000:36

  Info: virtio-mmio.devices=0x200@0x10200:37

  Info: virtio-mmio.devices=0x200@0x10400:38

[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct

...

[    2.153835] [drm] Initialized drm 1.1.0 20060810
[    2.160035] brd: module loaded
[    2.163355] loop: module loaded
[    2.164636] tun: Universal TUN/TAP device driver, 1.6
[    2.165668] tun: (C) 1999-2004 Max Krasnyansky <maxk@qualcomm.com>
[    2.170234] PPP generic driver version 2.4.2
[    2.171357] PPP BSD Compression module registered
[    2.172301] PPP Deflate Compression module registered
[    2.173316] PPP MPPE Compression module registered
[    2.174265] NET: Registered protocol family 24
[    2.175218] pegasus: v0.9.3 (2013/04/25), Pegasus/Pegasus II USB Ethernet driver
[    2.176743] usbcore: registered new interface driver pegasus
[    2.178115] usbcore: registered new interface driver rtl8150
[    2.179314] usbcore: registered new interface driver r8152
[    2.180464] usbcore: registered new interface driver asix
[    2.181593] usbcore: registered new interface driver ax88179_178a
guest $> uname -a
Linux 192.168.33.15 3.18.0 #1 SMP PREEMPT Thu Nov 16 19:56:15 CET 2017 aarch64 GNU/Linux
```

It is convient to note that it begins to make all sequence of boot. 


Finally if we debug the execution a bit, we can find several interesting things.

- The first time a *guest* instance is launched a hidden directory will be created ``/home/<user>/.lkvm/``. 	This directory has default filesystem of *guest* instance (``./default/``) and a file which specifies a socket, the name is like *<guest-name>.sock* if you specify a name or *guest-\<pid\>.sock* if you do not specify a name. If we execute ``netstat``, we can see the following:

```bash
host $> netstat -anp
  Proto RefCnt Flags       Type       State         I-Node   PID/Program name    Path
  ...
< unix  2      [ ACC ]     STREAM     LISTENING     57339    2318/lkvm           /root/.lkvm//foo.sock
```

The binary ``lkvm`` creates a socket because it uses the remote filesystem ``v9fs`` to synchronize the files between *host* and *guest*.  In addition, the filesystem ``v9fs`` is implemented by *Plan 9 9p* protocol which is used for distributed Linux applications. This protocol can be see on *guest* instance, for instance.

```bash
$ mount
mount: /proc/self/mountinfo: parse error: ignore entry at line 4.
/dev/root on / type 9p (rw,relatime,dirsync,trans=virtio,version=9p2000.L,cache=loose)
devtmpfs on /dev type devtmpfs (rw,relatime,size=50936k,nr_inodes=12734,mode=755)
hostfs on /host type 9p (ro,relatime,sync,dirsync,trans=virtio,version=9p2000.L)
proc on /proc type proc (rw,relatime)
devpts on /dev/pts type devpts (rw,relatime,mode=600,ptmxmode=000)
```

This also allows to make boot sequence from a virtual device with ``v9fs``filesystem, please note that the second line ``/dev/root on / type 9p `` there is a device mounted as ``/`` (*guest* root). In the description below there are the ``mount``details.

- ``rw`` mount the filesystem read/write.
- ``relatime`` update inode access time relative to modify or change time.
- ``dirsync`` all directory updates withing the filesystem should be done synchronously. 
- ``trans=virtio`` specifies the transport method, in this case [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README)  uses ``virtio``. 
- ``version=9p2000.L`` protocol version ``9p2000.L``
- ``cache=loose`` specifies cache mode, in this case allow loose caching semantics. The *guest* will attempt to flush the cache soon after a write to a file, it do it when an oplock (*opportunistic lock*) or lease is not held.

The information has been taken from ``man mount`` and [Documentation/9psetup](https://wiki.qemu.org/Documentation/9psetup) of QEMU wiki.

So from *host* side, we can access to *guest* filesystem, however from *guest* side can not access to *host* side. A dummy test is the following.

```bash
# guest console
$ touch /home/file.txt
$ sha1sum /home/file.txt
da39a3ee5e6b4b0d3255bfef95601890afd80709  /home/file.txt

# host console
$ sha1sum .lkvm/default/home/file.txt
da39a3ee5e6b4b0d3255bfef95601890afd80709  .lkvm/default/home/file.txt

```

Thas hashes matching correctly, so we can affirm that the files are the some.





## References <a name="References"></a>
- [Sasha Levin (Verizon Labs) - YouTube](https://www.youtube.com/watch?v=SsrfqHiQO4k&t=1028s)  
- [Asias He (Beihang University)- YouTube](https://www.youtube.com/watch?v=tIk3D58tpAU&t=41s)
- [Andre Przywara (ARM) - YouTube](https://www.youtube.com/watch?v=fLNTTXrWjbc)
- [GitHub kvmtool](https://github.com/clearlinux/kvmtool)  
- [Web HiKey - Running kvm guest in HiKey](https://www.96boards.org/blog/running-kvm-guest-hikey/)
- [Tracing KVM](http://www.linux-kvm.org/page/Tracing)

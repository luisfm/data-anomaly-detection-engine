/* 
 * Project name: DADE - Data Anomaly Detection Engine
 * Project Managers: José Luis Briz and Dar�o Su�rez - 
 * Computer Architecture Group, University of Zaragoza
 * Author: Jos� �Luis Briz
 * Date: 22 june 2018
 * File name: anorexic_mod.c
 *
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

int init_anorexic_mod(void) 
{
    printk(KERN_INFO "Hi!\n");
    /* != 0 means error */
    return 0;
}

void exit_anorexic_mod(void) 
{
    printk(KERN_INFO "Bye...\n"); 
}

module_init(init_anorexic_mod);
module_exit(exit_anorexic_mod);
MODULE_LICENSE("GPL");

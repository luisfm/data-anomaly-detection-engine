## Running *guest* instance

There are two script for running virtual machines. 

- ``run_guest.sh`` runs *enigma* machine with ``512 MB`` of memory, ``1`` vCPUs, and ``Image-3.18.0`` kernel.
- ``ftrace_run_guest.sh`` runs *pontifex* machine with ``254 MB`` of memory, ``5`` vCPUs and you must specify a kernel binary. Also, it runs *pontifex* with a few interesting kernel input parameters.
	- ``maxcpus=1`` kernel boots only with one vCPU. This option helps us to identify better stacktrace of kernel objetcs. The performance of booting decreases. 
	- ``nokaslr`` disables kernel and module base offsetn ASLR (*Address Space Layout Randomization.
	- ``tracer=nop`` enables ``nop`` tracer. It is a ``ftrace`` feature. 
	- ``trace_event=kmem:kmalloc`` enable tracing of ``kmalloc`` events. These events belongs to ``kmem`` module.
	- ``trace_options=stacktrace,sym-addr,noannotate,sym-offset`` enables the following ``nop`` tracer options
		- ``stacktrace`` registers a stack trace after any trace event. It is use for make the object kernel sign.
		- ``sym-addr`` displays the function address as well as the function name
		- ``noannotate`` not diplays when a new vCPU buffer started. It helps to parse generated ``trace`` file.
		- ``sym-offset`` displays the offset in the function. This option helps to better characterize the kernel object.

Now you can see a example of ``trace`` file with the above options.

```bash
  1 # tracer: nop                                                                 
  2 #                                                                             
  3 #                              _-----=> irqs-off                              
  4 #                             / _----=> need-resched                          
  5 #                            | / _---=> hardirq/softirq                       
  6 #                            || / _--=> preempt-depth                         
  7 #                            ||| /     delay                                  
  8 #           TASK-PID   CPU#  ||||    TIMESTAMP  FUNCTION                      
  9 #              | |       |   ||||       |         |                           
 10              cat-1257  [000] ....    38.975476: kmalloc: call_site=ffff0000082b503c ptr=ffff80000bdeb600 bytes_req=32 bytes_alloc=128 gfp_flags=GFP_KERNEL
 11              cat-1257  [000] ....    38.975478: <stack trace>                 
 12  => proc_open+0x28/0x40 [mutenroshi] <ffff000000b18028>                       
 13  => proc_reg_open+0x9c/0x13c <ffff0000082ff228>                               
 14  => do_dentry_open+0x22c/0x330 <ffff000008286004>                             
 15  => vfs_open+0x5c/0x8c <ffff00000828761c>                                     
 16  => path_openat+0x518/0xf78 <ffff00000829abb8>                                
 17  => do_filp_open+0x6c/0xe8 <ffff00000829c750>                                 
 18  => do_sys_open+0x14c/0x214 <ffff000008287a88>                                
 19  => SyS_openat+0x3c/0x4c <ffff000008287bd4>                                   
 20  => el0_svc_naked+0x34/0x38 <ffff000008083880>    
```
 
 
  

## References 
- *\<krsc\>/Documentation/admin-guide/kernel-parameters.txt*
- *\<krsc\>/Documentation/trace/ftrace.rst*

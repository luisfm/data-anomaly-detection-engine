#!/bin/bash

KERNEL="../kernels/Images/Image-3.18.0"
MEM="512"
VCPU="1"
NAME="enigma"

../src/kvmtool/lkvm run -k $KERNEL -m $MEM -c $VCPU --name $NAME
                         
exit 0

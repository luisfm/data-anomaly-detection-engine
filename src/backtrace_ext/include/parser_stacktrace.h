/*
 * Project name: DADE - Data Anomaly Detection Engine 
 * Project managers: José Luis Briz and Darío Suárez
 *                   Computer Architecture Group
 *                   University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 30 march 2018
 * File name: parser_stacktrace.h
 */

#ifndef DADE__PARSER_STACKTRACE
#define DADE__PARSER_STACKTRACE


#define EQUAL           1
#define FUNCTION        2
#define ADDR            3

#define MSG_LEN         8
#define SKIP_N_LINES    9
    
extern int parser_stacktrace__init(const char *filename, FILE *bbdd_fd, 
                                                         int id);


#endif /* DADE__PARSER_STACKTACE */

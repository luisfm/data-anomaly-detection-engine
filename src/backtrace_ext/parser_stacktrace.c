/*   
 * Project name: DADE - Data Anomaly Detection Engine 
 * Project managers: José Luis Briz and Darío Suárez - Computer Architecture Group  
 * University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 3 july 2018  
 * File name: parser_stacktrace.c 
 * Docs: https://www.gnu.org/software/libc/manual/html_node/Regular-Expressions.html 
 * 
 */       

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <regex.h> 
#include <linux/errno.h>

#include "linux/list.h" 
#include "parser_stacktrace.h"
#include "backtrace_ext.h"

#define DEBUG            0    

static int BEGIN_STACK;
static int COPY_OBJ_VALUE;
const char *delimiters         = " ";
static char *func_name, *obj_value, *first_obj_value;
static size_t func_name_len, obj_value_len;
static FILE *_bbdd_fd;
static _id;
regex_t regex_new_process, regex_ptr_name;
/* define and initializes a list_head named result_head.
 * The majority of the linked list routines accept one or
 * two parameters; the head node or the head node plus an
 * actual list node. Ref: Linux Kernel Development 
 */
static LIST_HEAD(result_list);
static int list_length;


/* 
 * parser_stacktrace__erase_list: clean [result_list] when new stacktrace 
 * will be created.
 * 
 * @result_list: the kernel list pointer
 * 
 */
static void parser_stacktrace__erase_list(struct list_head *result_list) 
{                        
    struct backtrace *ptr;                                                         
         
    list_for_each_entry(ptr, result_list, list) {                                  
        free(ptr->addr); 
        free(ptr->func_name); 
    }
}              


/* 
 * parser_stacktraec__state_machin: execute state machine to retrieve 
 * stacktrace; function name and address. Allocate these fields in 
 * [result_list].
 * 
 * @token: the word pointer
 * @trace: the backtrace struct pointer
 * @flag: the pointer which indicate what is [token] (equal, function name or
 * address)
 * 
 */
static int parser_stacktrace__state_machine(const char *token, 
                                            struct backtrace *trace, 
                                            int *flag) 
{
    size_t addr_len;
    
    if (*flag == EQUAL) {
        *flag = FUNCTION;
    } else if (*flag == FUNCTION) {
            
        func_name_len = strlen(token);
        func_name = malloc(sizeof(char) * (func_name_len + 1));
        if (func_name == NULL) {
            return -ENOMEM;
        }

        func_name[func_name_len] = '\0';
        strncpy(func_name, token, func_name_len);

        *flag = ADDR;
    } else if (*flag == ADDR) {
        /* token is [module_name] not an addr */
        if (token[0] == '[') {
            return;
        }

        trace->func_name = malloc(sizeof(char) * (func_name_len + 1));
        if (trace->func_name == NULL) {
            return -ENOMEM;
        }

        trace->func_name[func_name_len] = '\0';
        strncpy(trace->func_name, func_name, func_name_len); 

        addr_len = strlen(token);    
        trace->addr = malloc(sizeof(char) * (addr_len + 1));
        if (trace->addr == NULL) {
            return -ENOMEM;
        }
    
        strncpy(trace->addr, "0x", sizeof(char) * 3);
        trace->addr[addr_len] = '\0';
        strncat(trace->addr, &token[1], addr_len - 3);
    
        list_add(&trace->list, &result_list);
        list_length = list_length + 1;

        *flag = EQUAL;
    }

    return 0;
}


/* 
 * parser_stacktrace__allocate_object: allocate kernel object in backtrace 
 * extractor module [backtrace_ext__save_stacktrace].
 *
 */
static int parser_stacktrace__allocate_kobject(void) 
{
    int ret;

    if (first_obj_value != NULL) {
        free(first_obj_value);
    }

    first_obj_value = malloc(sizeof(char) * (obj_value_len - 1));
    if (first_obj_value == NULL) {
            return -ENOMEM; 
    }

    first_obj_value[obj_value_len - 2] = '\0';
    strncpy(first_obj_value, obj_value, obj_value_len - 2);
    ret = backtrace_ext__save_stacktrace(&result_list, list_length, 
                                          first_obj_value, obj_value_len, 
                                         _bbdd_fd, _id);
    if (ret < 0) {
        printf("Error when save stacktrace in backtrace extractor module\n");
        return -1;
    }


    return 0;
}


/* 
 * parser_stacktrace__clen_reg: parse a specific token and allocate kernel 
 * object.
 *
 * @token: the word pointer
 * @read: bytes read
 * 
 */
static int parser_stacktrace__clean_reg(char *token, const ssize_t read) 
{
    int ret;
    int *flag;
    const char msg [MSG_LEN] = {'t', 'r', 'a', 'c', 'e', '>', '\n', '\0'};
    struct backtrace *trace;
    
    /* flag = 1 equal. 
              2 func. name. 
              3 func. addr. */
    flag = malloc(sizeof(int *));
    *flag = EQUAL;
    while (token != NULL) {
    
        /* end of file, allocate last kernel object */
        if (read == -1) {
            ret = parser_stacktrace__allocate_kobject();        
            if (ret < 0) {
                return -1;
            }
            
            break;
        }

        /* end of stacktrace, allocate kernel object */
        ret = regexec(&regex_new_process, token, 0, NULL, 0);
        if (ret == 0 && token[0] != '[') {
            BEGIN_STACK = false;
        }

        /* allocate object name */
        if (!BEGIN_STACK) {

            ret = regexec(&regex_ptr_name, token, 0, NULL, 0);
            if (ret == 0) {

                if (COPY_OBJ_VALUE) {

                    ret = parser_stacktrace__allocate_kobject();        
                    if (ret < 0) {
                        return -1;
                    }

                    COPY_OBJ_VALUE = false;
                } 
    
                obj_value_len = strlen(token);
                obj_value = malloc(sizeof(char) * (obj_value_len - 1));        
                if (obj_value == NULL) {
                    return -ENOMEM; 
                }

                obj_value[2] = '\0';
                strncpy(obj_value, "0x", sizeof(char) * 2);
                obj_value[obj_value_len - 2] = '\0';
                strncat(obj_value, &token[4], obj_value_len - 4);
            }

        } else { 

            /* execute state machine */
            trace = (struct backtrace *) malloc(sizeof(struct backtrace));
            if (trace == NULL) {
                return -ENOMEM;
            }

            ret = parser_stacktrace__state_machine(token, trace, flag);
            if (ret < 0) {
                return -ENOMEM;
            }
        }

        /* begin again new stacktrace */
        if (strcmp(token, msg) == 0) {

            parser_stacktrace__erase_list(&result_list);
            list_del_init(&result_list);

            list_length = 0;
            COPY_OBJ_VALUE = true;
            BEGIN_STACK = true;
        }

        token = strtok(NULL, delimiters);
    }

    if (flag != NULL) {
        free(flag);
    }

    return 0;
}


/* 
 * parser_stacktrace__extract: read line by line and call [clean_reg] to par-
 * se tokens. 
 *
 * @fd: the ftrace filename pointer
 *
 */
static void parser_stacktrace__extract(FILE *fd) 
{
    char *line, *token;
    size_t len;
    ssize_t read;
    int skip;

    read = 0;
    skip = SKIP_N_LINES;
    while (read != -1) {

        read = getline(&line, &len, fd);
        /* skip ftrace file header */
        if (skip > 0) {
            skip = skip - 1;    
            continue;
        }

        token = strtok(line, delimiters);
        parser_stacktrace__clean_reg(token, read);
    }
}


/* 
 * parser_stacktrace__init: initialize function to parse ftrace file. 
 *
 * @filename: the ftrace filename pointer
 * @bbdd_fd: the descriptor pointer to write kernel objects
 *
 */
int parser_stacktrace__init(const char *filename, FILE *bbdd_fd, int id) 
{
    FILE *fd;
    const char *str_new_process = "^[[:blank:]]*(.*sh|init-1|kthreadd|"
    "<...>|kworker|kdevtmpfs|bash|<idle>|wc|cat|.*grep|dmesg|insmod|"
    "iptables)";
    const char *str_ptr_name     = "^[[:blank:]]*ptr";

    _id = id;
    _bbdd_fd = bbdd_fd;    
    fd = fopen(filename, "r");
    if (fd == NULL) {
        printf("File not open (check if exists or the path)\n");
        return -EBADR;
    }    
    
    regcomp(&regex_new_process, str_new_process, REG_EXTENDED);
    regcomp(&regex_ptr_name, str_ptr_name, REG_EXTENDED);

    BEGIN_STACK        = false;
    COPY_OBJ_VALUE    = false;
    parser_stacktrace__extract(fd);
    
    regfree(&regex_new_process);
    regfree(&regex_ptr_name);
    fclose(fd);

    return 0;
}

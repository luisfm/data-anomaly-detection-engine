# Data Anomaly Detection Engine (DADE)

The document is a summary of the DADE's memory, you can read it in the following document [doc/memory/Data_Anomaly_Detection_Engine_ENG.pdf](doc/memory/Data_Anomaly_Detection_Engine_ENG.pdf).

## Abstract 

Nowadays, low-volume servers prevail in data centers. They host virtual  machines which run multiple applications concurrently accessed by the users. Security is of paramount importance at both the application and operating system level, to avoid personal data leaks. If the system is compromised and an eavesdropper acquires rootly powers, her or his next move will be to perform a privilege rampage, either horizontal (inside the DMZ) or vertical (local net). A way to increase the privileges consists in installing malicious code in order to garble packet filtering and to access to other machines. There exist prototypes like *DADE: A fast Data Anomaly Detection Engine for kernel integrity monitoring* <cite>[9]</cite>, which help to detect code or data modification inside the operating system’s kernel. *DADE* provides a mechanism to maintain operating system integrity based on monitoring kernel objects. The created objects inside kernel can be characterized by stacktrace, which is the ordered trace of function invocations down to the kernel physical memory allocation interface. We have implemented the *DADE* prototype and contributed with new methods for detecting anomalies. Besides reproducing the kernel attack detection techniques exposed in the paper, we have implemented three new kernel attacks as a proof of concept to test the viability and effectiveness of the proposed technique.

This bachelor's thesis has been directed by [José Luis Briz](http://webdiis.unizar.es/~briz/) and [Darío Suárez](http://webdiis.unizar.es/~dario/) from [Computer Architecture Group of University of Zaragoza](http://webdiis.unizar.es/gaz/index.php).

## Table of Contents

1. [Introduction](#Introduction)
   1. [Motivation](#Motivation)
	2. [Goals](#Goals)
	3. [Scope](#Scope)
2. [Fundaments](#Fundaments)
	1. [Virtualization](#Virtualization)
	2. [Linux Kernel](#LinuxKernel)
		1. [Modules](#Modules)
		2. [Ftrace](#Ftrace)
3. [Testing Environment](#TestingEnv)
4. [Methodology](#Metho)
	1. [Proposed Architecture](#PropArch)
	2. [Object Characterization](#ObjectChar)
5. [Proofs of Concept](#PsoC)
	1. [Attack No. 1 Literal Pool Spoofing](#LiteralPool)
	2. [Attack No. 2 Unreliability Binary Script Format](#UnreliBin)
	3. [Attack No. 3 Garbling Netfilter IPv4](#GarblingNet)
6. [References](#References)


## 1. Introduction <a name="Introduction"></a>

### 1.1 Motivation <a name="Motivation"></a>

In recent years, computer security is taking on importance due to the new Spain Organic Data Protection Law (\acs{ODPL})  besides as by attacks produced currently. An example would be the six millions of Instagram accounts leaked (August 2017), DDoS attacks to Slovakian websites (June 2018), the intrusion into Liberty Secure's IT infrastructure (June 2018). In fact there are webpages like  [Norse Corp](http://www.norse-corp.com) which shown real time attacks. Most nodes are virtualized, that is, the operating system does not run on physical hardware. Furthermore, the use of virtual machines is widespread among cloud providers. One of the major concerns of providers, clients and researchers is security and integrity of code and data. Thus, when there is an eavesdropper it is necessary to mitigate a privilege scalation, either horizontally or vertically.


### 1.2 Goals <a name="Goals"></a>

The main goal is to complement defense mechanism relying on *hashing* techniques, such as those that can he applied to [Linux kernel](#LinuxKernel) modules. In order to achieve it, the techniques proposed by [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> will be applied so we will replicate *DADE* design. As well new detection techniques and proofs of concept will be provided.

### 1.3 Scope <a name="Scope"></a>

The achievement of goals requires to prepare a controlled environment to run virtual machines, as well as to conduncting a study of the physical kernel memory allocation interface. Once the virtual machines are up and running, *DADE* will be implemented through *backtrace-naming* technique which has been designed by [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> To this end, it is necessary to modify the virtual machine kernel by inserting instructions to debug memory allocation functions. When the virtual machine (*gues*t) needs to assign physical memory, the execution flow will pass to *Hypervisor* from which operations will be performed to extract the data required by *backtrace-naming*. The system which is described on [seminal article](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> disrupt on virtual machine performance due to it has to be done one world switch per physical memory allocation function. 

>Note: The context switch produced between *Hypervisor* and virtual machine (and viceversa) it is called *world switch*.

However Linux provides debugging mechanism to help to system developers. One of the most characteristic is [ftrace](https://www.kernel.org/doc/Documentation/trace/ftrace.txt), so this project will use it resorting to the technique described in [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> . [Ftrace](https://www.kernel.org/doc/Documentation/trace/ftrace.txt) <cite>[4]</cite> documentation has been studied to find the configuration which can extract the data to replicate *backtrace-naming* technique.

Once *DADE* prototype has been developed, we will deploy on it three proofs of concept which demonstrate that the design tecnhique detect the kernel attacks. These proofs has been carefully chosen to show the risk of inserting non-legitimate code into the operating system. Each attack carries a study of kernel structures and the modifications has been made in a precise way to change only what is necessary.


## 2. Fundaments <a name="Fundaments"></a>

[Virtualization](#Virtualization) and [Linux Kernel](#LinuxKernel) are introduced in this chapter. They are heavy concepts of this project. The \textit{Hypervisor} provide us an execution environment to setup virtual machines so kernel activity could be monitored to detect attacks.

### 2.1 Virtualization <a name="Virtualization"></a>

Virtualization is a technique which allow the execution of an operating system with its applications without a real hardware <cite>[4,7]</cite>. Figures [2.1](#figure_21) and [2.2](#figure_22) show the difference regarding to a physical machine.

<p align="center">
  <img id="figure_21" width="400" height="250" src="images/Diagrama_APSOHARD_EN.png">
</p>
<div>
Figure 2.1 Physical machine
</div>
&nbsp;

<p align="center">
  <img id="figure_22" width="400" height="250" src="images/Diagrama_APSOHYPSOHARD_EN.png">
</p>
<div>
Figure 2.2 Physical machine with Hypervisor and virtual machines
</div>
&nbsp;


Virtual machines made a big chance for IT companies and universities which had applications running on physical servers. These servers did not reach a full performance. The chance allowed multiple virtual machines to run on the same physical server with applications that need specific environments. This chance process is known as server consolidation and entails advantages.

* **Isolation and fault protection**. An operating system is executed in isolation on a virtual machine. If it fails, it must not affect to other virtual machines.

* **Agile development and multiplatform**. The applications need specific libraries or even differents library versions. Furthermore, the application provider could demand a specific opeating system version. Also virtual machines allow the development for  multiple platforms in an efficient way.

* **Security**. The isolation between physical (*host*) and virtual (*guest*) machine is crucial on production environments A infected virtual machine must not allow the propagation to other systems hosted on datacenter.

* **Running legacy applications**. Virtual machines could provide a legacy execution environment which enabling you to continue using these applications.


The software that makes this task possible is called *Hypervisor*, *Virtual Machine Monitor* (VMM) o *Control Program* (CP). From here it will be used *Hypervisor* to naming the software that creates virtual machines environment. Every machine, virtual or non-virtual, has that its own operating system offers to applications: processors, memory, disk space or I/O interface. These resources must be virtualized by a *Hypervisor* which is running on physical machine.

Gerald J. Popek et al <cite>[4]</cite> established three axioms that *Hypervisor* must fulfill to run virtual machines as if they were efficient duplicate and isolated from the real machine.

* **A1.** Provide an identical environment to applications as they are were running in physical machine.
* **A2.** The applications hosted in virtual machine, in the worst case scenario, they suffer decreases in execution speed.
* **A3.** All virtualized resources have to be under *Hypervisor* control.

A conventional machine from that age, like [IBM 360](https://en.wikipedia.org/wiki/IBM_System/360), has two execution modes: *supervisor* and *user*. On *supervisor* mode the processor could execute all instruction set architecture (ISA). On the physical machine operating system is executed on *supervisor* mode.Thus on *user* mode, the processor only could execute an instruction subset *unprivileged instructions*; the attempted execution of an not allowed (*privileged*) instruction results in an exception (e.g. status storage, context switch to *supervisor* mode and the jump to specific routine which could be *firmware* or belong to operating system. The applications are executed on this mode.

>Note: current microprocessors, specially general-purposes ones, have inherited these execution modes, exploited by operating systems. In some cases, they have more than two modes as Intelx86-64 which offers four. With that in mind, the operating system of virtual machine was set in *user* mode. This fact was a problem with sensitive non-privileged instructions. It was resolved by using binary translators (either static or dynamic). To solve this, hardware support for virtualization was added.


A *Hypervisor* must be executed in *supervisor* mode to have all instructions available, meanwhile their virtual machines will run on *user* mode including applications and operating system. In turn, it exists a subset inside *privilege* instructions; *sensitive* instructions. These instructions could be split into:

* *Sensitive control instructions*. They change system resources configuration. For instace, **Set CPU Timer** <cite>[8]</cite> (SPT, IBM System/370). It replaces the *timer* with a value referenced in memory if the CPU is in *supervisor* mode, otherwise, there is an interruption.
* *Sensitive behavioral instructions*. They change system resources configuration but their behaviour depends on processor configuration (*supervisor* or *user*). For instace, **Pop Stack into Flags Register** <cite>[8]</cite> (POPF, Intel IA-32). It writes state register flags the data extracted from the stack. On *user* mode, it overwrites all flags except *Interrupt-enable Flag* (IF). On *supervisor* mode, this flag could be modified.

FIgure [2.3](#figure_23) associates these instruction sets. *Sensitive* instructions modify the architectural state. According to **A3** virtualized system resource must be controlled by *Hypervisor*, hence every time that a *sensitive* instruction occurs, the *Hypervisor* Must be aware of it. Thus, the following theorem that Gerald J. Popek et al. <cite>[4]</cite> created comes up.


<p align="center">
  <img id="figure_23" width="300" height="280" src="images/inst_EN.png">
</p>
<div>
Figure 2.3 Instruction sets
</div>
&nbsp;


>**Theorem 1**. For every conventional third-generation computer, a Hypervisor can be built if the senstive instruction set is a subset of privilege instructions.

Therefore, any ISA which fulfills such theorem, could be virtualized by a *Hypervisor*. At first, *x86* architecture was not virtualized, however, virtualizations extensions ([Intel VT](https://www.intel.com/content/dam/www/public/us/en/documents/technology-briefs/virtualization-technology-connectivity-brief.pdf), [AMD-V](http://developer.amd.com/wordpress/media/2012/10/NPT-WP-1%201-final-TM.pdf), [ARM Arch64 Virtualization](https://static.docs.arm.com/100942/0100/aarch64_virtualization_100942_0100_en.pdf)) solved this issue.


### 2.2 Linux Kernel <a name="LinuxKernel"></a>

[Linux](https://www.kernel.org) was originally developed by Linux Torvalds in 1991. This operating system could run on IBM PC compatible which relying on Intel 80386 microprocessor.

>Note: IBM PC compatible or clones, refers to those PCs are architecturally equal to IBM PC, XT and AT. This goal was achieved by doing reverse engineering.

Nemeth et al <cite>[3]</cite> defined the Linux kernel as the responsible of resource distribution and the provider of services upon which user processes depend. The main Linux characteristics are the following:

* **Monolithic kernel**. All components (system call interace, filesystem, process control) are integrated in a single binary. Their execution is always in *privileged* mode. Once booting is finished, user process are executed in *non-privileged* mode. It is convenient to note that kernel code execution is triggered by exception.
* **Module support**. The kernel performs a dynamic link (and unlink) of kernel code portions when it is running. 
* **Kernel threading**. A *kernel thread* can be defined as an lightweight execution context inside kernel. They are created and executed in order to carry out a specific task, besides they can be associated  with user process.


> Note: kernel threads - init, keventd, kapmd, kswapd, kblockd.

* **Multithread processes**. A process can be made by various *threads* which share the same resources (address space, memory pages, open files). The threads that are created by system call ``clone`` are associated with a \textit{kernel threads} and they are called [LWP](https://en.wikipedia.org/wiki/Light-weight_process).

* **Kernel preemption**. Linux lets you configure the processes' eject of CPU. It can be done in different ways, with variations throughout their versions, that improve efficiency on whether it is a server, a personal computer or a real time system. Thus, in totally expulsive models (*full preemption*), a process running on *kernel* mode can be eject of CPU and replaced by another by an interruption which invokes the scheduler. *Full preemtion* model can be implemented in variety of ways; current model ``PREEMPT_DESKTOP``, the eject is allowed if the process is not in a critical section. Real time systems (Linux *preempt_rt patch*) would be a extreme circumstance, even the exception service routines and deferred functions (*softirqs*, *tasklets*, *workqueues*) are scheduled. They cannot be interlaced. In the most restrictive model *partial preemption*, (``PREEMPT_NONE``) a process which has been executed on *kernel* mode, only will yield the CPU when it invokes the scheduler directly with ``schedule`` function (direct invocation) or invoking it when it is ready to return to *user* mode (*lazy invocation*).


Other important issue is physical memory allocation of virtual machine. Figure  [2.4](#figure_24) shows the [mapping between virtual and physical machine]((https://events.static.linuxfound.org/images/stories/pdf/lcna_co2012_marinas.pdf)). When a virtual machine is launched, KVM performs kernel pages allocation
from the ``0x0000004000000000`` address. This space belongs to KVM user space. Therefore kernel objects will be mapped on those pages. This information can be found on [\<ksrc\>/Documentation/arm64/memory.txt](https://www.kernel.org/doc/Documentation/arm64/memory.txt).

<p align="center">
  <img id="figure_24" width="450" height="560" src="images/aarch64_mem_layout.png">
</p>
<div>
Figure 2.4 Virtual memory mapping <i>AArch64</i>
</div>
&nbsp;

KVM is a type 1.7 *Hypervisor*, so it is running as user process in operating system of physical machine (*host*). It is the key concept, hence, the virtualized system (*guest*) lives on KVM user space. Thus, virtual machine kernel needs protection mechanisms which it would have when running on a real machine, so a virtualization support is necessary.

#### 2.2.1 Modules <a name="Modules"></a>

The modules are part of code which can be dynamically link (and unlink) while the kernel is running. This linked is interactively performed or by *shell script*. They allow to extend the features without reconfigure, recompile and link stactically kernel components, besides as the need to generate a new binary image and reboot the machine. Therefore they allow to add or remove hardware or virtual *drivers*, monitoring utilities, filesystems support.

On this project, the attacks have been performed by injecting modified modules in order to produce abnormal behaviours inside kernel. To detect these attacks, a tool that provides the kernel itself has been used. The tool is [Ftrace](#Ftrace).


#### 2.2.2 Ftrace <a name="Ftrace"></a>


[Ftrace](https://elixir.bootlin.com/linux/v4.18.5/source/Documentation/trace/ftrace.rst) can be defined as a debugger that allows system programmers see events inside kernel. Furthermore it is used on latency analysis and performance of kernel space. The Ftrace configuration is done in ``/sys/kernel/debug/tracing`` directory. This directory owns ``tracefs`` filesystem.

> Note: Tracefs is [introduced in Linux kernel 4.13](https://lwn.net/Articles/630526/), before debugfs was used but it was discarded for security reasons.

In order to manage this tool, it is necessary to change kernel configuration. Please, if you need to know the options go to [./doc/README.md](doc/README.md) (*Guest kernel configuration*).


## 3. Testing Environment <a name="TestingEnv"></a>

The tools which have been used on this project have focused to cost-saving and simplicity execution of proofs of concept.  

* **Hardware**. The project has been implemented on [Hikey LeMaker](http://www.lemaker.org/product-hikey-index.html) development board. It has been *2GB LPDDR3 SDRAM*, *SoC Kirin 620* with a *CPU ARM-A53O Octa-core 64-bit* up to 1.2GHz and *GPU Mali450-MP4*. The instruction set is ARMv8. The board provides the necessary resources to run virtual machine in a simple and economical way. [Figure](#figure_31) shows the board.


<p align="center">
  <img id="figure_31" width="650" height="860" src="images/hikey_lemaker.jpg">
</p>
<div>
Figure 3.1 HiKey LeMaker
</div>
&nbsp;

* **Software** [*backtrace extractor*](src/backtrace_ext/backtrace_ext.c) binary is implemented in C and the [*integrity verifier*](src/integrity_verifier/integrity_verifier.awk) script AWK. [Kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git) has been used to launch virtual machines effortlessly and quickly. This tool allows to run low-resource virtual machines. It is a *Hypervisor* much easier than [QEMU](https://www.qemu.org) [KVM](https://git.kernel.org/pub/scm/virt/kvm/kvm.git), or [Xen](https://xenproject.org/). It is intended for different purposes: 

	- Kernel development and debugging of guest instances.
	- *Symmetric Multi-Processing*^(SMP) applications.
	- Study basic concepts of virtualization.


    Both issues are appealing for kernel developers, distributed and parallel systems developers and virtualization engineers. It supports virtual machines of the same host-system architecture, as well as Linux [*Real-Time patch*](https://rt.wiki.kernel.org/index.php/Main_Page). It also can run 32-bit applications in 64-bit kernel. If you need to launch a virtual machine instance, firstly you must compile a kernel with the following [Guest kernel configuration](doc/README.md).

> Notes: AWK came from creators' surnames: Alfred **A**ho, Peter **W**einberger y Brian **K**ernighan. [Kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git) has been implemented by Pekka Enberg, Cyrill Gorcunov, Asias He, Sasha Levin and Prasad Josh with the help of Avi Kivity e Ingo Molnar. Nowadays, the latest *commits* are made by Julien Thierry, Jean-Philippe Brucker and Andre Przywara.

## 4. Methodology <a name="Metho"></a>

At first, the chapter defines [developed architecture](#PropArch) and then [object kernel characterization](#ObjectChar) is explained. The architecture can be integrated in a *Hypervisor* as [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git). 

*DADE* aims to detect physical memory allocation trazability anomalies of kernel objects, therefore the [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> view has been applied. So virtual machine kernel will remain unchanged. Also other fields will be provided to perform a more accurate characterization. It will help to apply *machine learning* (as [SVM](http://bibing.us.es/proyectos/abreproy/11185/fichero/Volumen+1_Detector+Multiusuario+para+DS-CDMA+basado+en+SVM%252F7.+Support+Vector+Machines%252FSupport+Vector+Machines.pdf)) techniques in a future work.

### 4.1 Proposed Architecture <a name="PropArch"></a>

Figure [Figure](#figure_41) shows the detection engine architecture which recognizes rootkits attacks. There are two significantly componentes **backtrace extractor** which charactizes the object and **integrity verifier** which performs anomaly detection between two objects. In order to extract meaningful fields, debugging file ``/sys/kernel/debug/tracing/trace`` must be parsed and then these fields need to be inserted in **backtrace extractor**. In this way, the verifier match object to object and detects if there is any incongruence of kernel objects' *hash*. The hash is generated with function addresses. 

<p align="center">
  <img id="figure_41" width="690" height="470" src="images/DADE_diagram.png">
</p>
<div>
Figure 4.1 <i>DADE</i> scheme and steps
</div>
&nbsp;

A *parser* which is in charge of ``ftrace`` file cleaning, forms the **backtrace extractor**. The file contains a ordered function list invoked until ``kmalloc`` execution.

>Note: ``kmalloc`` allocates physical memory for kernel objects which are smaller than page size.

Also the file contains function addresses, *offsets* and kernel object name. These fields allow to make an accurate object characterization. The executed operations are the following:


* **Step no. 1** - ``ftrace`` is up and running on virtual machine boot sequence. It begins to write in ``trace`` file.
* **Step no. 2** - virtual machine filesystem is regularly monitored inr order to check if ``trace`` file exists. 
* **Step no. 3** - if file exists, extractor launch a thread which executes the initialization function of ``parser_stacktrace`` binary. The function extracts significant fields, for instance  object name, all *stacktrace* (with addresses) and *offsets*, as well as the value of reserved memory address.
	* Before all fields have been extracted, they are given to ``backtrace_extractor`` which fills ``backtrace_container`` struct. It also makes the *hash* sign of functions addresses. 
	* Finally the extractor writes ``backtrace_container`` struct in ``kobjects{i}.data``file. 

You can see [backtrace extractor](src/backtrace_extractor/backtrace_ext.c) and [*parser*](src/backtrace_extractor/parser_stacktrace.c) code. The **integriy verifier** compares objects *hash* signs as follows:

* **Step no. 4** - verifier input parameters are two files generated by extractor (``kobjects{i}.data`` and ``kobjects{i+1}.data``).
* At beginning, it takes the filename of first parameter in order to distinguish the arrays of each one. The array uses object name as index and *hash* sign as value. 
* Then, object name ``obj_name`` is read and ``key`` variable stores it. 
* **Step no. 5** - When ``hash` is found, it inserts the sign as value and object name as index.
* **Step no. 6** - Finally if the reading of files ended, it performs a walk over arrays in order to search the object. If the object exists in both, it compares *hash* signs to search an incongruence. So if two *hash* signs differ from each other, the attack has been successfully detected.

You can see [integrity verifier](src/integrity_verifier/integrity_verifier.awk) code to compare it with the different steps.


### 4.2 Object Characterization <a name="ObjectChar"></a>

According to object characterization, [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> resort to *backtrace-naming* identification technique. It consists in register the active functions chains (with their addresses) that are in process or thread stack. In order to define object context, only the backtrace corresponding to the cascade of memory allocation functions, needs to be kept in mind. So the technique is based on three points:

* Most kernel objects are created through a specific set of allocating functions.
* Kernel context at the time an object is created reflects the object characteristics at runtime.
* Object modificattion during an attack means modifying the normal trace with which it is accessed. So if we save *name* (e.g. the access trace), afterwards we can compare it to traces allegedly modified by an attack, detecting the fact.

[Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> extracts *backtraces* by modifying memory allocation kernel functions. The modification is made with a *hypercall* function which is a call to *Hypervisor* context. In this way, when virtual machines kernel needs to allocate memory, it will cause an exception and the flow execution will pass to *Hypervisor* in order to perform the changes and extract *backtrace*. Figure [4.2](#figure_42) shows the explained process. 

<p align="center">
  <img id="figure_42" width="650" height="460" src="images/DADE_hypercall.png">
</p>
<div>
Figure 4.2 Diagram <i>hypercall</i> solution
</div>
&nbsp;

This mechanism produces a degradation of the virtualized system. This is because of a context switch is made to *Hypervisor* when a single kernel object is allocated.

[Ftrace](#Ftrace) is a contribution of this project which is used to obtain object characterization. Furthermore it manages to reduce the latencies that are produced by context switch between *Hypervisor* - virtual machine and viceversa. The *stacktrace* (``echo stacktrace > /sys/kernel/debug/tracing/trace_options``) is automatically generated by [ftrace](#Ftrace), as well as the functions addresses (``echo sym-addr > /sys/kernel/debug/tracing/trace_options``). The following lines show *inode* *backtrace*:

```bash
2900  iptables-1283  [000] ....   176.885405: kmalloc: \ 
      call_site=ffff0000089ff5f8 ptr=ffff80001bfb7100 \
      bytes_req=128 bytes_alloc=128 gfp_flags=GFP_KERNEL
2901  iptables-1283  [000] ....   176.885408: <stack trace>                 
2902  => alloc_inode+0x2c/0xb8 <ffff0000082b7114>                                  
2903  => new_inode_pseudo+0x20/0x60 <ffff0000082b9398>                             
2904  => sock_alloc+0x28/0xd8 <ffff0000089fe5d8>                                   
2905  => __sock_create+0x5c/0x1b8 <ffff0000089feeec>                               
2906  => SyS_socket+0x58/0xe8 <ffff000008a00420>    
```

At first, an in-depth study of debugging events has been realised and ``kmalloc`` (``echo 1 > /sys/kernel/debug/tracing/events/kmem/kmalloc/enable``) tracing has been enabled. This function allocates kernel objects smaller than page size. Another contribution is the addition of *offset* (``echo sym-offset > /sys/kernel/debug/tracing/trace\_options``) field which indicates where the function is called. So object characterization is more deterministic (it suffers fewer variations between executions) when the detection is performed.

## 5. Proofs of Concept <a name="PsoC"></a>

The experiments show how anomaly detection can be performed on virtual machines. This goal can be achivied by [backtrace extractor](src/backtrace_extractor/backtrace_ext.c) and [integrity verifier](src/integrity_verifier/integrity_verifier.awk). The proofs of concept are own because [Hayoon Yi et al.](https://link.springer.com/content/pdf/10.1007%2Fs11227-017-2131-6.pdf) <cite>[9]</cite> article does not publish the code. 


### 5.1 Attack No. 1 Literal Pool Spoofing <a name="LiteralPool"></a>

The attack modifies [Linux kernel](#LinuxKernel) by inserting an instruction which loads a constant into a register. In ARM, the constant values that exceed 16 bits can not be load by ``mov``-immediate instruction and they are stored into literal pool. If the literal pool is modified, the jump addresses will be disturbed so the execution flows will change.

> Notes: [PC-relative modes](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.den0024a/CJAIFJII.html) (load-literal). The literal pool is a block of data encoded in an instruction stream. They are at the end of every function and usually store basic blocks adresses.

[*Mutenroshi*](src/rootkits/literal_pool/) kernel module creates a file in ``/proc`` directory. The  ``kmalloc`` function will be execute when the file is read. Then the function calls tree will be traced by [ftrace](#Ftrace). We force the ``kmalloc`` invoking function as follows: 

```bash
$ cat /proc/mutenroshi
kamehameha!
```

> Note: proc pseudo-filesystem is mounted on ``/proc`` directory. This directory provides hardware information and kernel management (processes, I/O devices, modules, CPU).


The code which forms the illegitimate module is implemented by adding an assembly instruction. This instruction loads the ``3462381247`` value in ``w0`` register, therefore the compiler will create literal pool and execute the load by making a PC relative addresing that will point to literal pool.

```c
 26 static int proc_open(struct inode *inode, struct file *file)                  
 27 {                                                                             
 28     __asm__("LDR w0, =3462381247");                                           
 29     return single_open(file, proc_show, NULL);                                
 30 }
```

Table [5.1](#table_51) shows two *stacktraces*: ``proc_open`` invokes ``single_open`` which finally invokes ``kmalloc`` function. It is convenient to note that ``proc_open`` function owns an *offset* equal to ``0x28/0x40`` which is relative to PC. However when it inserts the instruction which loads a random value into literal pool, the *offset* changes to ``0x2c/0x40``.

<table id="table_51" style="width:100%">
  <tr>
    <th>good_mutenroshi.ko</th>
    <th>bad_mutenroshi.ko</th> 
  </tr>
  <tr>
    <td>el0_svc_naked+0x34/0x38 </td>
    <td>el0_svc_naked+0x34/0x38</td> 
  </tr>
  <tr>
    <td>SyS_openat+0x3c/0x4c</td>
    <td>SyS_openat+0x3c/0x4c</td> 
  </tr>
  <tr>
    <td>do_sys_open+0x14c/0x214</td>
    <td>do_sys_open+0x14c/0x214</td>
  </tr>
  <tr>
    <td>do_filp_open+0x6c/0xe8</td>
    <td>do_filp_open+0x6c/0xe8</td>
  </tr>
  <tr>
    <td>path_openat+0x518/0xf78</td>
    <td>path_openat+0x518/0xf78</td>
  </tr>
  <tr>
    <td>vfs_open+0x5c/0x8c</td>
    <td>vfs_open+0x5c/0x8c</td>
  </tr>
  <tr>
    <td>do_dentry_open+0x22c/0x330</td>
    <td>do_dentry_open+0x22c/0x330 </td>
  </tr>
  <tr>
    <td>proc_reg_open+0x9c/0x13c </td>
    <td>proc_reg_open+0x9c/0x13c</td>
  </tr>
  <tr>
    <td>proc_open+0x28/0x40</td>
    <td>proc_open+0x2c/0x40</td>
  </tr>
  <tr>
    <td>single_open+0x78/0xc0</td>
    <td>single_open+0x78/0xc0</td>
  </tr>
</table>
<div>
Table 5.1 Literal pool spoofing <i>stacktrace</i>
</div>
&nbsp;


We need to consider the following assembly code in order to understand the impact. The instructión nº 18 of illegitimate module loads into ``w0`` register the literal stored in ``proc_show+0x48``. Thus it causes that ``single_open`` return address is *+4* higher than the legitimate module.

> Note: ``w0`` is ARMv8 32 bit register.

```assembly
good_mutenroshi.ko: file format elf64-littleaarch64  

Disassembly of section .text:  

<proc_open>:                                                 
0: stp x29, x30, [sp,#-32]!                                  
4: mov x29, sp                                               
8: str x19, [sp,#16]                                         
c: mov x0, x30                                               
10: mov x19, x1                                               
14: bl  0 <_mcount>                                           
18: ldr x1, 38 <proc_open+0x38>                               
1c: mov x2, #0x0                
20: mov x0, x19                                               
24: bl  0 <single_open>                                       
28: ldr x19, [sp,#16]                                         
2c: ldp x29, x30, [sp],#32                                    
30: ret                                                       
34: nop                                                       
...        
``` 

```assembly
bad_mutenroshi.ko: file format elf64-littleaarch64 

Disassembly of section .text:                                                                       
<proc_open>:                                                      
0: stp x29, x30, [sp,#-32]!                                       
4: mov x29, sp                                                    
8: str x19, [sp,#16]                                              
c: mov x0, x30                                                    
10: mov x19, x1                                                    
14: bl  0 <_mcount>                                                
18: ldr w0, 88 <proc_show+0x48>                                    
1c: ldr x1, 38 <proc_open+0x38>                                    
20: mov x2, #0x0                     
24: mov x0, x19                                                    
28: bl  0 <single_open>                                            
2c: ldr x19, [sp,#16]                                              
30: ldp x29, x30, [sp],#32                                         
34: ret                                                            
...
<proc_show>:   
...
88: .word   0xce5fbebf                                             
8c: nop                  
```

[Backtrace extractor](src/backtrace_extractor/backtrace_ext.c) obtains Table [5.2](#table_52) *hashes* that correspond to ``single_open`` object.

<table id="table_52" style="width:100%">
  <tr>
    <th>Kernel object</th>
    <th>MD5 hash</th> 
  </tr>
  <tr>
    <td><code>single_open</code></td>
    <td>90ffd3626bbdc6a9b964909f2ba0fb8c </td> 
  </tr>
  <tr>
    <td><code>single_open</code></td>
    <td>7fb1c3bd6212987c57443729982940b4</td> 
  </tr>
</table>
<div>
Table 5.2 <code>single_open</code> object <i>hashes</i>
</div>
&nbsp;


### 5.2 Attack No. 2 Unreliability Binary Script Format <a name="UnreliBin"></a>

The attack entails the insertion of malicious procedure in ``load_script`` function which belongs to ``binfmt_script`` kernel module. This module is in charge of running script format. The target of this attack is to jump to the inserted procedure every time that function is executed, so execution flow will be modified and the node will be degraded.

> Note: ``load_script`` function checks *#!* characters.

According to Evi Nemeth <cite>[3]</cite> Linux kernel supports diverse executable file formats ([ELF](https://www.cs.cmu.edu/afs/cs/academic/class/15213-s00/doc/elf.pdf), [COFF](http://www.ti.com/lit/an/spraao8/spraao8.pdf)). It also allows the binary execution belonging to other operating systems (*MS-DOS*, Unix *BSD* family). The formats are recognized by ``magic number`` which is encoded in the first 128 bytes of all files. 

The illegitimate module with procedure call can be observed in line 98. Full code is found in [bad_binfmt_script.c](src/rootkits/binfmt_script/src/bad_binfmt_script_3.c).

```c
17 static int load_script(struct linux_binprm *bprm)                             
18 {  
       ...
98     get_max_files();
99     /*                                                                        
100     * OK, now restart the process with the
          interpreter's dentry.             
101     */                                                                       
102     file = open_exec(i_name);                                                 
103     if (IS_ERR(file))                                                         
104         return PTR_ERR(file);       
        ...
121     return 0;                                                                 
122 }            
```

Table [5.3](#table_53) are gathered the *stacktraces* of both the unmodified and malicious module, that correspond to the *script* execution. The formatted *stackatraces* that [backtrace extractor](src/backtrace_extractor/backtrace_ext.c) generates are found in [data](src/rootkits/binfmt_script/data) directory.

<table id="table_53" style="width:100%">
  <tr>
    <th>good_binfmt_script.ko</th>
    <th>bad_binfmt_script.ko</th> 
  </tr>
  <tr>
    <td>el0_svc_naked+0x34/0x38</td>
    <td>el0_svc_naked+0x34/0x38</td> 
  </tr>
  <tr>
    <td>SyS_execve+0x38/0x4c</td>
    <td>SyS_execve+0x38/0x4c</td> 
  </tr>
  <tr>
    <td>do_execve+0x44/0x54</td>
    <td>do_execve+0x44/0x54</td>
  </tr>
  <tr>
    <td>do_execveat_common.isra.37+0x494/0x63c</td>
    <td>do_execveat_common.isra.37+0x494/0x63c</td>
  </tr>
  <tr>
    <td>search_binary_handler+0xb0/0x204</td>
    <td>search_binary_handler+0xb0/0x204</td>
  </tr>
  <tr>
    <td>load_script+0x1f8/0x218</td>
    <td>load_script+0x1fc/0x218 </td>
  </tr>
  <tr>
    <td>search_binary_handler+0xb0/0x204</td>
    <td>search_binary_handler+0xb0/0x204</td>
  </tr>
</table>
<div>
Table 5.3 unreliability binary script format <i>stacktrace</i>
</div>
&nbsp;

Legitimate ``load_script`` function owns a *offset* equal to ``0x1f8/0x218`` meanwhile illegitimate function owns a ``0x1fc/0x218``. It is important to note that the object *hashes* [5.4](#table_54) are different.

<table id="table_54" style="width:100%">
  <tr>
    <th>Kernel object</th>
    <th>MD5 hash</th> 
  </tr>
  <tr>
    <td><code>search_binary_handler </code></td>
    <td>17e39e204a228aff9da4c2d657b34a68</td> 
  </tr>
  <tr>
    <td><code>search_binary_handler</code></td>
    <td>8ebd6a46aa779b90cd651b708a475226</td> 
  </tr>
</table>
<div>
Table 5.4 search_binary_handler object <i>hashes</i>
</div>
&nbsp;


### 5.3 Attack No. 3 Garbling Netfilter IPv4 <a name="GarblingNet"></a>


The proposed attack is based on modification of ``ip_tables`` module which belongs to Netfilter *framework*. This module performs the *ipv4* packet filtering. The attack's target is to change the IP address which the user wants to filter. In this way the malicious IP will not be added to *firewall* rules.


The attack's implications are dangerous because the user writes right IP and the malicious code change it performing a bitwise ``NOT`` operation. Firstly, the user should to know what module is failing and the Netfilter structures in order to fix this problem. In advance the user can not view the code, so it should be disassembly.

``FILTER`` table is a Netfilter component which is used to filter packets. This table only filter packets, it does not modify them. In order to use ``FILTER`` table, we need to load three modules in the following order due to the dependence between them:

> Note: There are other tables: ``NAT``, ``MANGLE``, ``CONNTRACK``.

1. **x_tables** ``<ksrc>/net/netfilter/x_tables.c`` - performs the generic filter based on protocol-independent table (*ipv4*, *ipv6*, *arp*, *eb*).
2. **ip_tables** ``<ksrc>net/ipv4/netfilter/ip_tables.c`` - performs the rule record on *ipv4* table for ``iptables`` tool. This tool is in charge of user commnand line configuration.
3. **iptable_filter** ``<ksrc>/net/ipv4/netfilter/iptables_filter.c`` - initialzes the function which jumps to **ip_tables** module in order to allocate memory and record the table. Furthermore it inializes the ``LOCAL_IN``, ``LOCAL_OUT`` and ``FORWARD`` hooks because they are necessary to filter *ipv4* packet.


The *rootkit* implementation is in ``translate_table`` function, more specifically on 740-745 lines.

```c

 734     /* Finally, each sanity check must pass */                                
 735     i = 0;                                                                    
 736     xt_entry_foreach(iter, entry0, newinfo->size) {                           
 737         ret = find_check_entry(iter, net, repl->name, 
             repl->size,             
 738         &alloc_state);                                         
 739                                                                               
 740         if (((iter->ip.src.s_addr >> 24U) & 255) != 0 &&                      
 741             ((iter->ip.src.s_addr >> 16U) & 255) != 0 &&                      
 742             ((iter->ip.src.s_addr >> 8U) & 255) != 0 &&                       
 743              (iter->ip.src.s_addr & 255) != 0) {                              
 744             iter->ip.src.s_addr = ~iter->ip.src.s_addr;                       
 745         }                                                                     
 746                                                                               
 747         if (ret != 0)                                                         
 748             break;                                                            
 749         ++i;                                                                  
 750     }
 751                                                                               
 752     if (ret != 0) {                                                           
 753         xt_entry_foreach(iter, entry0, newinfo->size) {                       
 754             if (i-- == 0)                                                     
 755                 break;                                                        
 756             cleanup_entry(iter, net);                                         
 757         }                                                                     
 758         return ret;                                                           
 759     }                                                                         
 760                                                                               
 761     return ret;                                                               
 762  out_free:                                                                    
 763     kvfree(offsets);                                                          
 764     return ret;                                                               
 765 }                                                                             
 766                                        
```

The main function goal is to perform a information sanitize which stored on ``entry0`` field. These field belongs to ``xt_table_info`` structure which is which is pointed by the ``newinfo`` variable. Table [5.5](#table_55) contains the *stacktraces*.


<table id="table_55" style="width:100%">
  <tr>
    <th>good_ip_tables.ko</th>
    <th>bad_ip_tables.ko</th> 
  </tr>
  <tr>
    <td>el0_svc_naked+0x34/0x38</td>
    <td>el0_svc_naked+0x34/0x38</td> 
  </tr>
  <tr>
    <td>SyS_setsockopt+0x74/0xd0 </td>
    <td>SyS_setsockopt+0x74/0xd0</td> 
  </tr>
  <tr>
    <td>sock_common_setsockopt+0x54/0x68</td>
    <td>sock_common_setsockopt+0x54/0x68</td>
  </tr>
  <tr>
    <td>raw_setsockopt+0x70/0xb0</td>
    <td>raw_setsockopt+0x70/0xb0</td>
  </tr>
  <tr>
    <td>ip_setsockopt+0x7c/0xa8</td>
    <td>ip_setsockopt+0x7c/0xa8</td>
  </tr>
  <tr>
    <td>nf_setsockopt+0x64/0x88</td>
    <td>nf_setsockopt+0x64/0x88</td>
  </tr>
  <tr>
    <td>do_ipt_set_ctl+0x1ac/0x248</td>
    <td>do_ipt_set_ctl+0x1ac/0x238</td>
  </tr>
  <tr>
    <td>__do_replace+0xe4/0x250</td>
    <td>__do_replace+0xe4/0x250 </td>
  </tr>
</table>
<div>
Table 5.5 garbling Netfilter <i>ipv4 stacktrace</i>
</div>
&nbsp;


The *offset* ``0x1ac/0x238`` of illegitimate module is less than ``0x1ac/0x248`` which belongs to legitimate module. These *offsets* appear like this because the compiler performs optimization with ``-O2`` so the code has been reorderer in order to make the necessary optimizations. The ``__do_replace`` *hashes* are shown on Table [5.6](#table_56)


<table id="table_56" style="width:100%">
  <tr>
    <th>Kernel object</th>
    <th>MD5 hash</th> 
  </tr>
  <tr>
    <td><code>__do_replace</code></td>
    <td>1f827b15780378d9e4f6cbfb5f4e0455</td> 
  </tr>
  <tr>
    <td><code>__do_replace</code></td>
    <td>0910c045af680e7f4b5b422288c498f7</td> 
  </tr>
</table>
<div>
Table 5.6 __do_replace object <i>hashes</i>
</div>
&nbsp;



## 6. References <a name="References"></a>

[1] Jan Engelhardt and Nicolas Bouliane. «Writing Netfilter Modules». (2012), pages 4-5.

[2] Peter J. Denning. «Third Generation Computer Systems». *Commputing Surveys* 3.4 (1971), page 177.

[3] Trent R. Hein Ben Whaley y Dan Mackin Evi Nemeth Garth Snyder. *Unix and Linux System Administration Handbook*. 5.th ed. Boston, MA, USA: Addison–Wesley, 2017. Cap. 20 - Program Executon, pages 824-826.

[4] Gerald J. Popek and Robert P. Goldberg. «Formal Requirements for Virtualizable Third Generation». *Communications of the ACM* 17.7 (1974), page 413.

[5] Rusty Russell y Harald Welte. «Linux Netfilter Hacking HOW- TO». (2002), pages 5-7.

[6] James E. Smith and Ravi Nair. *Virtual Machines: Versatile Platforms for Systems and Processes*. Amsterdan, Netherlands: Elsevier, 2005. Chap. 1 - Introduction to Virtual Machines, pages 1-6.

[7] James E. Smith and Ravi Nair. *Virtual Machines: Versatile Platforms for Systems and Processes*. Amsterdan, Netherlands: Elsevier, 2005. Chap. 8 - System Virtual Machines, pages 384-385.

[8] Yunheung Paek and Kwangman Ko Hayoon Yi Yeongpil Cho. «DADE: a Fast Data Anomaly Detection for Kernel Integrity Monitoring». *The Journal of Supercomputing* (2017), pages 8-10.



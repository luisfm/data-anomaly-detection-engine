#!/bin/bash

BARGS=85

if [ -z "$1" ] ; then
    echo "$0 <kernel>"
    exit -$BARGS 
fi

KERNEL=$1
MEM="254"
VCPU="5"
NAME="pontifex"

# kernel command line arguments
KDEBUG="maxcpus=1 nokaslr tracer=nop trace_event=kmem:kmalloc trace_options=stacktrace,sym-addr,noannotate,sym-offset"

../src/kvmtool/lkvm run -k $KERNEL -m $MEM -c $VCPU --name $NAME -p "$KDEBUG" 

exit 0

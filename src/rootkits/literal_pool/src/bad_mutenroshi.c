/*   
 * Project name: DADE - Data Anomaly Detection Engine 
 * Project managers: José Luis Briz and Darío Suárez - Computer Architecture Group  
 * University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 27 june 2018  
 * File name: good_mutenroshi.c
 */   


#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#define KMEM        512
#define PROC_NAME   "mutenroshi"
#define MSG         "kamehameha!"

static int proc_show(struct seq_file *m, void *v)
{
    seq_printf(m, "%s\n", MSG);
    return 0;
}

static int proc_open(struct inode *inode, struct file *file)
{
    __asm__("LDR w0, =3462381247");
    return single_open(file, proc_show, NULL);
}

static struct file_operations fops= {
    .owner = THIS_MODULE,
    .open = proc_open,
    .release = single_release,
    .read = seq_read,
    .llseek = seq_lseek,
};

static int __init mutenroshi_init(void)
{
    struct proc_dir_entry *proc_file;

    proc_file = proc_create(PROC_NAME, 0644, NULL, &fops);
    if (proc_file == NULL) {
        return -ENOMEM;    
    } else {
        printk(KERN_INFO "Hi, I'm %s rootkit.\n", PROC_NAME);
    }

    return 0;
}

static void __exit mutenroshi_exit(void)
{
    remove_proc_entry(PROC_NAME, NULL);
    printk(KERN_INFO "Bye...\n");
}

module_init(mutenroshi_init);
module_exit(mutenroshi_exit);
MODULE_LICENSE("GPL");

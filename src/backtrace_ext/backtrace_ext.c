/* 
 * Project name: DADE - Data Anomaly Detection Engine
 * Project managers: JosÃ© Luis Briz and DarÃ­o SuÃ¡rez 
 * Computer Architecture Group - University of Zaragoza
 * Author: Luis Fueris MartÃ­n
 * Date: 3 july 2018
 * File name: backtrace_ext.c
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/list.h>
#include <string.h>
#include <linux/errno.h>
#include <openssl/md5.h>
#include <time.h>
#include <pthread.h>

#include "backtrace_ext.h"
#include "parser_stacktrace.h"

#define DEBUG        0

static int kernel_obj_num[MAX_THREADS];
static int nfile = 0;
pthread_mutex_t nfile_lock;


/* 
 * debug__print_list: debug purposes, print kernel list which contains addr 
 * and function name.
 *  
 * @result_list: the stacktrace list poiinter
 * 
 */
static void debug__print_list(const struct list_head *result_list) 
{
    struct backtrace *ptr;
    int i;

    i = 0;
    list_for_each_entry(ptr, result_list, list) {
        /* when assign [ºbtr_list = *result_list], the list head act as 
         * circular linked list. */
        if (ptr->func_name == NULL || ptr->addr == NULL) {
            break;
        }

        printf("[%d] %s -> %s\n", i, ptr->addr, ptr->func_name);
        printf("\t  :\n");
        printf("\t  :\n");
        i = i + 1;
    }

    printf("\t  := Kernel object has been allocated.\n\n");
}


/* 
 * debug__print_dock: debug purposes, print dock structure 
 * 
 */
static void debug__print_dock(struct backtrace_container dock) 
{
    printf("dock.obj_name:  \n\t%s\n", dock.obj_name);
    printf("dock.obj_value (ptr): \n\t%s\n", dock.obj_value);
    printf("dock.hash: \n\t%s\n", dock.hash);
}


/* 
 * backtrace_ext__write_data: save stacktrace in [fd] descriptor.
 *
 * @fd: the kernel object bbdd descriptor pointer
 *
 */
static void backtrace_ext__write_data(struct backtrace_container dock, 
                                      FILE *fd, int id) 
{
    struct backtrace *ptr;
    int i;
    
    fprintf(fd, "\nKernel object No. %d:\n\n", kernel_obj_num[id]);
    fprintf(fd, " obj_name: %s\n", dock.obj_name);
    fprintf(fd, " obj_value: %s\n", dock.obj_value);
    fprintf(fd, " hash: %s\n", dock.hash);
    fprintf(fd, "\n\t\t\t\t\t stacktrace\n");
    fprintf(fd, "\t\t\t\t\t(kernel trap)\n\n");

    i = 0;
    list_for_each_entry(ptr, &dock.btr.list, list) {
        if (ptr->func_name == NULL || ptr->addr == NULL) {
            break;
        }

        fprintf(fd, " [%d] %s - %s\n", i, ptr->func_name, ptr->addr); 
        i = i + 1;
    }

    fprintf(fd, "-----------------------------------------------------------"
                "------------------\n");
    if (dock.obj_name != NULL) {
        free(dock.obj_name);
    }

    if (dock.obj_value != NULL) {
        free(dock.obj_value);
    }
}


/* 
 * backtrace_ext__generate_hash: generate signature with function addresses. 
 * 
 * @dock: the dock container pointer
 * 
 */
static void backtrace_ext__generate_hash(struct backtrace_container *dock) 
{
    /* 16 (MD5_DIGEST_LENGTH) * 8b (char) = 128b */
    unsigned char digest[MD5_DIGEST_LENGTH];
    /* 128b / 4b = 32 hex chars */
    char hash[MD5_DIGEST_LENGTH * 2 + 1];        
    char *all_addr;
    struct backtrace *ptr;
    int i;

    all_addr = malloc(sizeof(char) * BUFSIZE);
    list_for_each_entry(ptr, &dock->btr.list, list) {
        if (ptr->addr == NULL || 
            ptr->func_name == NULL) {
            break;
        } 
        
        strncat(all_addr, ptr->addr, sizeof(char) * (ADDR_LEN + 2));
    }

    MD5((unsigned char*)all_addr, sizeof(char) * strlen(all_addr), 
                                                    (unsigned char*)&digest);    
    /* 02x means if your provided value is less than two digits then 0 
       will be prepended. */
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&hash[i * 2], "%02x", (unsigned int)digest[i]);
    }

    strncpy(dock->hash, hash, MD5_DIGEST_LENGTH * 2);

    if (all_addr != NULL) {
        free(all_addr);
    }
}


/* 
 * backtrace_ext__copy_obj_name: extract kernel object name and put it in
 * [dock] struct. 
 * 
 * @dock: the dock container pointer
 * @list_length: list size
 *
 */
static void backtrace_ext__copy_obj_name(struct backtrace_container *dock, 
                                            const int list_length) 
{
    struct backtrace *ptr;
    size_t obj_name_len;
    int i;

    i = 0;
    list_for_each_entry(ptr, &dock->btr.list, list) {
        if (i == (list_length - FUNC_OBJ_NAME)) {

            obj_name_len = strlen(ptr->func_name);
            dock->obj_name = malloc(sizeof(char) * (obj_name_len + 1));
            dock->obj_name[obj_name_len] = '\0';
            strncpy(dock->obj_name, ptr->func_name, obj_name_len);

            break;
        }

        i = i + 1;
    }
}


/*  
 * backtrace_ext__save_stacktrace: save stacktrace in [bbdd_fd] desriptor.
 * This procedure is called from parser stacktrace module, In particular
 * from [parser_stacktrace__allocate_kobject].
 * 
 * @result_list: the stacktrace list pointer
 * @list_length: list size
 * @obj_value: it is the [ptr] field in Ftrace file
 * @obj_value_len: obj value size (but it has got fixed size? check it)
 * @bbdd_fd: the kernel object bbdd descriptor pointer
 * 
 */
int backtrace_ext__save_stacktrace(const struct list_head *result_list, 
                                const int list_length, const char* obj_value, 
                                const int obj_value_len, FILE *bbdd_fd, 
                                const int id) 
{
    struct backtrace_container dock;
    kernel_obj_num[id] = kernel_obj_num[id] + 1;

    INIT_LIST_HEAD(&dock.btr.list);
    dock.hash[MD5_DIGEST_LENGTH * 2] = '\0';
    
    dock.obj_value = malloc(sizeof(char) * (obj_value_len - 1));
    dock.obj_value[obj_value_len - 2] = '\0';
    strncpy(dock.obj_value, obj_value, obj_value_len - 2);

    dock.btr.list = *result_list;
    backtrace_ext__copy_obj_name(&dock, list_length);
    backtrace_ext__generate_hash(&dock);

    backtrace_ext__write_data(dock, bbdd_fd, id); 

    return 0;
}


/* 
 * backtrace_ext__init: create kobject[0-9]+.data file, open Ftrace file and 
 * call parser stacktrace module [parser_stacktrace__init] to clean Ftrace 
 * file.
 *
 * @filename: the Ftrace file pointer
 * 
 */
static void *backtrace_ext__init(void *filename) 
{
    pthread_mutex_lock(&nfile_lock);
    int id = nfile;
    const char bbdd_name[BBDD_NAME_LEN] = { 'k', 'o', 'b', 'j', 'e', 'c', 
                    't', 's', nfile + '0', '.', 'd', 'a', 't', 'a', '\0' };
    pthread_mutex_unlock(&nfile_lock);

    int ret;
    time_t local_time;
    char *local_time_str;
    FILE *bbdd_fd;
    pthread_t tid;

    local_time = time(NULL);
    local_time_str = ctime(&local_time);

    bbdd_fd = fopen(bbdd_name , "w"); 
    if (bbdd_fd == NULL) {
        printf("File not open in backtrace_ext__init\n");
        pthread_exit((void*) -EBADR);
    }

    fprintf(bbdd_fd, "Date %s\n", local_time_str);
    tid = pthread_self();

    kernel_obj_num[id] = 0;
    ret = parser_stacktrace__init(filename, bbdd_fd, id);
    if (ret < 0) {
        printf("Error in parser extractor module\n");
        fclose(bbdd_fd);
    }

    fclose(bbdd_fd);
    pthread_exit(NULL);
}


/* 
 * backtrace_ext__messages: show init messages.
 * 
 */ 
static void backtrace_ext__messages(void) 
{
    printf("\n\t\t\tData Anomaly Detection Engine (DADE)\n\n");
    printf("\t\tAuthors: Jose Luis Briz, Luis Fueris and Dario "
                                                        "Suarez\n\n\n");
    printf("Backtrace extractor is running!\n\n");
    printf("[*] Checking if ftrace file exists in default filesystem\n");
}


/* 
 * main: check Ftrace file exists in default filesystem (/root/.lkvm/default/
 * virt/trace) and launch thread in [backtrace_ext__init] function.
 * 
 */
int main(int argc, char *argv[]) 
{
    const char *filename = "/root/.lkvm/default/virt/trace";
    char ftrace_filename[FTRACE_NAME_LEN] = 
                                    { 't', 'r', 'a', 'c', 'e', ' ', '\0'  };
    int ret, nthreads, i;
    pthread_t threads[MAX_THREADS];

    ret = pthread_mutex_init(&nfile_lock, NULL);
    if (ret != 0) {
        printf("Mutex initialization failed...\n");
        return -1;
    }

    backtrace_ext__messages();
    nthreads = 0;
    while (true) {

        printf("[!] Ftrace file doesn't exist...\n");
        sleep(5);
        if (access(filename, F_OK) != -1) {

            printf("\n[*] Ftrace file has been found\n");
            sleep(3);

            ftrace_filename[5] = nfile + '0';

            /* move/rename file and launch thread */
            rename(filename, ftrace_filename);
            ret = pthread_create(&threads[nthreads], NULL, 
                        backtrace_ext__init, (void *) ftrace_filename);
            if (ret) {
                printf("[!] Thread failed...\n");
            } else {
                printf("--> Thread No. %d with TID %d has been launched to " 
                        "parse ftrace file!\n\n", nfile, threads[nthreads]);
            }

            nthreads = nthreads + 1;

            pthread_mutex_lock(&nfile_lock);
            nfile = nfile + 1;
            pthread_mutex_unlock(&nfile_lock);

            printf("\n[*] Checking if ftrace file exists in default "
                                                            "filesystem\n");
        } 
    }

    for (i = 0; i < nthreads; i++) {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&nfile_lock);
    
    return 0;
}

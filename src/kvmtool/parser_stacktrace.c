/*   
 * Project name: DADE - Data Anomaly Detection Engine 
 * Project managers: José Luis Briz and Darío Suárez - Computer Architecture Group  
 * University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 30 march 2018  
 * File name: parser_stacktrace.c 
 * Docs: https://www.gnu.org/software/libc/manual/html_node/Regular-Expressions.html 
 * */       

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h> 
#include <linux/errno.h>
#include <linux/list.h>

#include "dade/parser_stacktrace.h"
#include "dade/backtrace_ext.h"

#define DEBUG			 0	


int BEGIN_STACK = 0;
int COPY_OBJ_VALUE = 0;
char *func_name;
char *obj_value, *first_obj_value;
size_t func_name_len, obj_value_len;
const char *delimiters = " ";
regex_t regex_new_process, regex_ptr_name;
/* define and initializes a list_head named result_head.
 * The majority of the linked list routines accept one or
 * two parameters; the head node or the head node plus an
 * actual list node. Ref: Linux Kernel Development 
 */
static LIST_HEAD(result_list);
int list_length;


/* procedure to clean the list */
void parser_stacktrace__erase_list(struct list_head *result_list) {                            
                                                                                   
    struct backtrace *ptr;                                                         
         
    list_for_each_entry(ptr, result_list, list) {                                  
		free(ptr->addr);                                                                                  
		free(ptr->func_name); 
    }

}              

/* execute state machine to retrieve stacktrace (function name, address) */
int parser_stacktrace__state_machine(char *token, struct backtrace *trace, int *flag) {

	size_t addr_len;
	
	if (*flag == EQUAL) {
		*flag = FUNCTION;
	} else if (*flag == FUNCTION) {
			
		func_name_len = strlen(token);
		func_name = malloc(sizeof(char) * (func_name_len + 1));
		if (func_name == NULL) {
			return -ENOMEM;
		}

		func_name[func_name_len] = '\0';
		strncpy(func_name, token, func_name_len);

		*flag = ADDR;
	} else if (*flag == ADDR) {

		trace->func_name = malloc(sizeof(char) * (func_name_len + 1));
		if (trace->func_name == NULL) {
			return -ENOMEM;
		}

		trace->func_name[func_name_len] = '\0';
		strncpy(trace->func_name, func_name, func_name_len); 

		addr_len = strlen(token);	
		trace->addr = malloc(sizeof(char) * (addr_len + 1));
		if (trace->addr == NULL) {
			return -ENOMEM;
		}
		
		strncpy(trace->addr, "0x", sizeof(char) * 3);
		trace->addr[addr_len] = '\0';
		strncat(trace->addr, &token[1], addr_len - 3);
	
		list_add(&trace->list, &result_list);
		list_length = list_length + 1;

		*flag = EQUAL;
	}

	return 0;
}

/* allocate kernel object in backtrace extractor */
int parser_stacktrace__allocate_kobject(void) {
	
	int ret;

	if (first_obj_value != NULL) {
		free(first_obj_value);
	}

	first_obj_value = malloc(sizeof(char) * (obj_value_len - 1));
	if (first_obj_value == NULL) {
			return -ENOMEM; 
	}

	first_obj_value[obj_value_len - 2] = '\0';
	strncpy(first_obj_value, obj_value, obj_value_len - 2);
	/* save all data */
	ret = backtrace_ext__save_stacktrace(&result_list, list_length, 
											first_obj_value, obj_value_len);
	if (ret < 0) {
		printf("Error when save stacktrace in backtrace extractor module\n");
		return -1;
	}


	return 0;
}

/* parser_stacktrace__clen_reg: function which parse the line. */
int parser_stacktrace__clean_reg(char *token, ssize_t read) {

	int ret;
	int *flag;
	const char *msg;
	struct backtrace *trace;
	
	/* flag = 1 equal. 2 func. name. 3 func. addr. */
	flag = malloc(sizeof(int *));
	*flag = EQUAL;
	msg = "trace>\n";
	while (token != NULL) {

		if (read == -1) {

			ret = parser_stacktrace__allocate_kobject();		
			if (ret < 0) {
				return -1;
			}

			break;
		}

		/* not execute state machine, first we retrieve object name */
		ret = regexec(&regex_new_process, token, 0, NULL, 0);
		if (ret == 0) {
			BEGIN_STACK = 0;
		}

		/* object name */
		if (!BEGIN_STACK) {

			ret = regexec(&regex_ptr_name, token, 0, NULL, 0);
			if (ret == 0) {
				if (COPY_OBJ_VALUE) {

					ret = parser_stacktrace__allocate_kobject();		
					if (ret < 0) {
						return -1;
					}

					COPY_OBJ_VALUE = 0;
				} 
	
				obj_value_len = strlen(token);
				obj_value = malloc(sizeof(char) * (obj_value_len - 1));		
				if (obj_value == NULL) {
					return -ENOMEM; 
				}

				obj_value[2] = '\0';
				strncpy(obj_value, "0x", sizeof(char) * 2);
				obj_value[obj_value_len - 2] = '\0';
				strncat(obj_value, &token[4], obj_value_len - 4);
			}

		} else { /* execute state machine */

			trace = (struct backtrace *) malloc(sizeof(struct backtrace));
			if (trace == NULL) {
				return -ENOMEM;
			}

			ret = parser_stacktrace__state_machine(token, trace, flag);
			if (ret < 0) {
				return -ENOMEM;
			}
		}

		/* begin again new stacktrace */
		if (strcmp(token, msg) == 0) {

			parser_stacktrace__erase_list(&result_list);
			list_del_init(&result_list);

			list_length = 0;
			COPY_OBJ_VALUE = 1;
			BEGIN_STACK = 1;
		}

		token = strtok(NULL, delimiters);
	}

	if (flag != NULL) {
		free(flag);
	}

	return 0;
}

/* read line by line and extract stacktrace */
void parser_stacktrace__extract(FILE *fd) {

	char *line, *token;
	size_t len;
	ssize_t read;
	int skip;

	read = 0;
	skip = SKIP_N_LINES;
	while (read != -1) {
		read = getline(&line, &len, fd);
		/* skip the header */
		if (skip > 0) {
			skip = skip - 1;	
			continue;
		}
			
		token = strtok(line, delimiters);
		parser_stacktrace__clean_reg(token, read);
    }

}

/* function which open the file trace and extract it*/
int parser_stacktrace__init(char *filename) {

	FILE *fd;
	const char *str_new_process = 
	"^[[:blank:]]*(sh|init-1|wc|kthreadd|<...>|kworker|kdevtmpfs|bash|<idle>)";
	const char *str_ptr_name = "^[[:blank:]]*ptr";

	fd = fopen(filename, "r");
	if (fd == NULL) {
		printf("File not open (check if exists or the path)\n");
		return -EBADR;
	}	
	
	regcomp(&regex_new_process, str_new_process, REG_EXTENDED);
	regcomp(&regex_ptr_name, str_ptr_name, REG_EXTENDED);

	parser_stacktrace__extract(fd);
	
	regfree(&regex_new_process);
	regfree(&regex_ptr_name);
	fclose(fd);

	return 0;
}


# README

These directories contain the code of different *DADE* pieces. A short structure explanation is the folllowing:

* ``asm/`` - contains a assembly program to test ARMv8 debug registers. It was not used in this project.
* ``backtrace_ext/`` - contains the **backtrace extractor** and stacktrace *parser*. 
	* ``data/`` - ``backtrace_ext`` sanitize files.
	* ``include/`` - project header files and libraries.
	* ``logs/`` - ``backtrace_ext`` and ``parser_stacktrace`` miscellaneous logs. 
	* ``stacktraces/`` - ``ftrace`` generated files.
* ``integrity_verifier`` - contains **integrity_verifier** script. 
	* ``data/`` - ``backtrace_ext`` sanitize files. 

* ``kvmtool/`` - [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/) hypervisor. 
* ``rootkits/`` - *DADE* proofs of concept.
	* ``binfmt_script/`` - attack no. 1 *Literal Pool Spoofing*.
	* ``literal_pool/`` - attack no. 2 *Unreliability Binary Script Format*.
	* ``module_template/`` - kernel module template.
	* ``netfilter_framework/`` - attack no. 3 *Garbling Netfilter IPv4*.
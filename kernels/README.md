## kernel binaries


The kernels that has been used to virtualize are from official [Linux](https://www.kernel.org) source code. Moreover to generate the binary you need to follow the following documentation [Building Debian from Source Code](http://wiki.lemaker.org/HiKey(LeMaker_version):Building_Debian_from_Source_Code), Section 2 *Build kernel from source*. If you have an Intel machine and you need to compile an ARM kernel, please download GCC cross-toolchain from Linaro to do cross-compiling. There are two options, [32 bit](https://releases.linaro.org/archive/15.02/components/toolchain/binaries/arm-linux-gnueabihf/) or [64 bit](https://releases.linaro.org/archive/15.02/components/toolchain/binaries/aarch64-linux-gnu/). One is for generate 32 bit kernel and other for 64 bit kernel respectively, pick the one you want. 

Now we can view an example of install cross-compiling toolchain and compiling of kernel (previously you must download toolchain described above).

```bash
$ mkdir arm-tc arm64-tc
$ tar --strip-components=1 -C ${PWD}/arm64-tc -xf gcc-linaro-4.9-*-x86_64_aarch64-linux-gnu.tar.xz
$ tar --strip-components=1 -C ${PWD}/arm-tc -xf gcc-linaro-4.9-*-x86_64_arm-linux-gnueabihf.tar.xz
$ export PATH="${PWD}/arm-tc/bin:${PWD}/arm64-tc/bin:$PATH"
```

Please, note that ``-j`` option specifies the number of commands to execute in parallel. If you have four cores and one thread per core, you can specify ``-j4``. The example below we use fifty-six cores.

```bash
$ make distclean 
$ make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- defconfig 
$ make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j56 Image modules 
```

It is convenient to qualify that you can change ``.config`` file before execute last ``make`` command. After the compilation is complete, the binary generated is named ``Image`` and is under *<ksrc>/arch/arm64/boot/*. In this case for ARM 64 bits.

An example of execution with [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) can be as following:

```bash
$ lkvm run -k ./data-anomaly-detection-engine/kernels/Image-4.14.47-1 -m 254 -c 2 --name foo
 # lkvm run -k ./data-anomaly-detection-engine/kernels/Image-4.14.47-1 -m 254 -c 2 --name foo
  Info: Loaded kernel to 0x80080000 (12040672 bytes)
  Info: Placing fdt at 0x8fc00000 - 0x8fdfffff
  Info: virtio-mmio.devices=0x200@0x10000:36

  Info: virtio-mmio.devices=0x200@0x10200:37

  Info: virtio-mmio.devices=0x200@0x10400:38

[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct

...

```

More information about [kvmtool](https://git.kernel.org/pub/scm/linux/kernel/git/will/kvmtool.git/tree/README) and parameters is on [./doc/kvmtool.md](./doc/kvmtool.md).


## References
1. [HiKey(LeMaker version):Building Debian from Source Code](http://wiki.lemaker.org/HiKey(LeMaker_version):Building_Debian_from_Source_Code)
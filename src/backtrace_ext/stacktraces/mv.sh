#! /bin/bash


for name in `ls traces/` ; do
	
	if [[ "$name" =~ \.trace ]] ; then
		continue
	else
		mv traces/${name} traces/${name}.trace
	fi

done

exit 0

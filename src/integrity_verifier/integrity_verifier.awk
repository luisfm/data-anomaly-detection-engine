#! /usr/bin/awk -f
# Project name: DADE - Data Anomaly Detection Engine
# Project managers: José Luis Briz and Darío Suárez  
# Computer Architecture Group, Univesity of Zaragoza
# Author: Luis Fueris Martín
# Date: 27 august 2018
# File name: integrity_verifier.awk
# Docs: https://wwww.gnu.org/software/gawk/manual/gawk.html

BEGIN {

    BARGS = 85
    
    if (ARGC < 3) {
        print ENVIRON["_"], "kobject0.data kobject1.data";
        exit BARGS;
    }

    f_file = ARGV[1];
    
}

$1 ~ /obj_name/ { 

    split($2, obj_names, "+");
    key = obj_names[1];

}

$1 ~ /hash/ {

    hash = $2;
    FILENAME == f_file ? v0_kobjects[key] = hash : v1_kobjects[key] = hash;

}

END {

    for (v0_key in v0_kobjects) {
        for (v1_key in v1_kobjects) {
            if (v0_key == v1_key && 
                v0_kobjects[v0_key] != v1_kobjects[v1_key]) {

                print "\n[!] Rootkit has been detected\n";
                print "\tKernel object:", v0_key, "\n";
                print "Hashes no. 1: ", v0_kobjects[v0_key];
                print "Hashes no. 2: ", v1_kobjects[v1_key];
            }
        }
    }
}


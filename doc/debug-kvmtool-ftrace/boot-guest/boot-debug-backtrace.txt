Script started on Tue 27 Feb 2018 07:52:42 PM UTC
root@linaro-alip:~/DADE/scripts# clear./run_guest.sh ../kernels/Image4-debug 
  # lkvm run -k ../kernels/Image-4.4.0 -m 254 -c 2 --name pontifex
  Info: Loaded kernel to 0x80080000 (12040672 bytes)
  Info: Placing fdt at 0x8fc00000 - 0x8fdfffff
  Info: virtio-mmio.devices=0x200@0x10000:36

  Info: virtio-mmio.devices=0x200@0x10200:37

  Info: virtio-mmio.devices=0x200@0x10400:38

[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
[    0.000000] Linux version 3.18.0 (leviathan@latitude) (gcc version 4.9.2 20140904 (prerelease) (crosstool-NG linaro-1.13.1-4.9-2014.09 - Linaro GCC 4.9-2014.09) ) #1 SMP PREEMPT Thu Nov 16 19:56:15 CET 2017
[    0.000000] CPU: AArch64 Processor [410fd033] revision 3
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] efi: Getting EFI parameters from FDT:
[    0.000000] efi: UEFI not found.
[    0.000000] cma: Reserved 128 MiB at 0x0000000087c00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv0.2 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] PERCPU: Embedded 14 pages/cpu @ffffffc00fdd9000 s19328 r8192 d29824 u57344
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 64135
[    0.000000] Kernel command line:  console=ttyS0 rw rootflags=trans=virtio,version=9p2000.L,cache=loose rootfstype=9p init=/virt/init  ip=dhcp
[    0.000000] PID hash table entries: 1024 (order: 1, 8192 bytes)
[    0.000000] Dentry cache hash table entries: 32768 (order: 6, 262144 bytes)
[    0.000000] Inode-cache hash table entries: 16384 (order: 5, 131072 bytes)
[    0.000000] Memory: 101872K/260096K available (7665K kernel code, 650K rwdata, 3080K rodata, 356K init, 493K bss, 158224K reserved)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vmalloc : 0xffffff8000000000 - 0xffffffbdffff0000   (   247 GB)
[    0.000000]     vmemmap : 0xffffffbe00000000 - 0xffffffbfc0000000   (     7 GB maximum)
[    0.000000]               0xffffffbe01c00000 - 0xffffffbe01f79000   (     3 MB actual)
[    0.000000]     PCI I/O : 0xffffffbffa000000 - 0xffffffbffb000000   (    16 MB)
[    0.000000]     fixed   : 0xffffffbffbdfe000 - 0xffffffbffbdff000   (     4 KB)
[    0.000000]     modules : 0xffffffbffc000000 - 0xffffffc000000000   (    64 MB)
[    0.000000]     memory  : 0xffffffc000000000 - 0xffffffc00fe00000   (   254 MB)
[    0.000000]       .init : 0xffffffc000b00000 - 0xffffffc000b59000   (   356 KB)
[    0.000000]       .text : 0xffffffc000080000 - 0xffffffc000aff434   ( 10750 KB)
[    0.000000]       .data : 0xffffffc000b59000 - 0xffffffc000bfb9e0   (   651 KB)
[    0.000000] Preemptible hierarchical RCU implementation.
[    0.000000] 	RCU restricting CPUs from NR_CPUS=64 to nr_cpu_ids=2.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=2
[    0.000000] NR_IRQS:64 nr_irqs:64 16
[    0.000000] Architected cp15 timer(s) running at 1.20MHz (virt).
[    0.000004] sched_clock: 56 bits at 1200kHz, resolution 833ns, wraps every 3579139414016ns
[    0.000255] Console: colour dummy device 80x25
[    0.002280] allocated 4194304 bytes of page_cgroup
[    0.002284] please try 'cgroup_disable=memory' option if you don't want memory cgroups
[    0.002295] Calibrating delay loop (skipped), value calculated using timer frequency.. 2.40 BogoMIPS (lpj=12000)
[    0.002304] pid_max: default: 32768 minimum: 301
[    0.002389] Security Framework initialized
[    0.002402] SELinux:  Initializing.
[    0.002463] Mount-cache hash table entries: 512 (order: 0, 4096 bytes)
[    0.002469] Mountpoint-cache hash table entries: 512 (order: 0, 4096 bytes)
[    0.002935] Initializing cgroup subsys memory
[    0.002957] Initializing cgroup subsys devices
[    0.002969] Initializing cgroup subsys freezer
[    0.002978] Initializing cgroup subsys net_cls
[    0.002985] Initializing cgroup subsys blkio
[    0.002994] Initializing cgroup subsys perf_event
[    0.003003] Initializing cgroup subsys net_prio
[    0.003010] Initializing cgroup subsys hugetlb
[    0.003020] Initializing cgroup subsys debug
[    0.003250] /cpus/cpu@0: Unknown CPU type
[    0.003260] /cpus/cpu@1: Unknown CPU type
[    0.003306] hw perfevents: enabled with arm/armv8-pmuv3 PMU driver, 1 counters available
[    0.003332] EFI services will not be available.
[    0.085413] CPU1: Booted secondary processor
[    0.085433] Detected VIPT I-cache on CPU1
[    0.085752] Brought up 2 CPUs
[    0.085769] SMP: Total of 2 processors activated.
[    0.086483] devtmpfs: initialized
[    0.091190] xor: measuring software checksum speed
[    0.190959]    8regs     :  2384.800 MB/sec
[    0.291229]    8regs_prefetch:  2177.200 MB/sec
[    0.391535]    32regs    :  2641.600 MB/sec
[    0.491839]    32regs_prefetch:  2320.400 MB/sec
[    0.491845] xor: using function: 32regs (2641.600 MB/sec)
[    0.491882] pinctrl core: initialized pinctrl subsystem
[    0.492342] regulator-dummy: no parameters
[    0.493249] NET: Registered protocol family 16
[    0.516129] cpuidle: using governor ladder
[    0.542057] cpuidle: using governor menu
[    0.542182] vdso: 2 pages (1 code @ ffffffc000b61000, 1 data @ ffffffc000b60000)
[    0.542290] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    0.544264] software IO TLB [mem 0x85c00000-0x86000000] (4MB) mapped at [ffffffc005c00000-ffffffc005ffffff]
[    0.545440] DMA: preallocated 256 KiB pool for atomic allocations
[    0.545548] Serial: AMBA PL011 UART driver
[    0.752582] raid6: int64x1    476 MB/s
[    0.923026] raid6: int64x2    698 MB/s
[    1.093569] raid6: int64x4   1010 MB/s
[    1.263683] raid6: int64x8   1024 MB/s
[    1.433908] raid6: neonx1     743 MB/s
[    1.604126] raid6: neonx2    1148 MB/s
[    1.774329] raid6: neonx4    1520 MB/s
[    1.944518] raid6: neonx8    1508 MB/s
[    1.944524] raid6: using algorithm neonx4 (1520 MB/s)
[    1.944529] raid6: using intx1 recovery algorithm
[    1.945028] vgaarb: loaded
[    1.945372] SCSI subsystem initialized
[    1.945864] usbcore: registered new interface driver usbfs
[    1.945920] usbcore: registered new interface driver hub
[    1.945991] usbcore: registered new device driver usb
[    1.946204] Linux video capture interface: v2.00
[    1.946552] Advanced Linux Sound Architecture Driver Initialized.
[    1.947049] NetLabel: Initializing
[    1.947055] NetLabel:  domain hash size = 128
[    1.947059] NetLabel:  protocols = UNLABELED CIPSOv4
[    1.947099] NetLabel:  unlabeled traffic allowed by default
[    1.947295] Switched to clocksource arch_sys_counter
[    1.978464] NET: Registered protocol family 2
[    1.979064] TCP established hash table entries: 2048 (order: 2, 16384 bytes)
[    1.979092] TCP bind hash table entries: 2048 (order: 3, 32768 bytes)
[    1.979129] TCP: Hash tables configured (established 2048 bind 2048)
[    1.979169] TCP: reno registered
[    1.979178] UDP hash table entries: 256 (order: 1, 8192 bytes)
[    1.979194] UDP-Lite hash table entries: 256 (order: 1, 8192 bytes)
[    1.979390] NET: Registered protocol family 1
[    1.979763] RPC: Registered named UNIX socket transport module.
[    1.979772] RPC: Registered udp transport module.
[    1.979776] RPC: Registered tcp transport module.
[    1.979780] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    1.980211] kvm [1]: HYP mode not available
[    1.983224] futex hash table entries: 512 (order: 3, 32768 bytes)
[    1.983304] audit: initializing netlink subsys (disabled)
[    1.983346] audit: type=2000 audit(1.969:1): initialized
[    1.983959] HugeTLB registered 2 MB page size, pre-allocated 0 pages
[    1.984476] VFS: Disk quotas dquot_6.5.2
[    1.984532] Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.985197] NFS: Registering the id_resolver key type
[    1.985228] Key type id_resolver registered
[    1.985233] Key type id_legacy registered
[    1.985425] fuse init (API version 7.23)
[    1.985751] 9p: Installing v9fs 9p2000 file system support
[    1.985804] msgmni has been set to 454
[    1.989150] io scheduler noop registered
[    1.989214] io scheduler cfq registered (default)
[    1.994482] Serial: 8250/16550 driver, 4 ports, IRQ sharing disabled
[    1.995894] console [ttyS0] disabled
[    1.996009] of_serial 3f8.U6_16550A: ttyS0 at MMIO 0x3f8 (irq = 32, base_baud = 115200) is a 16550A
[    2.144349] console [ttyS0] enabled
[    2.145872] of_serial 2f8.U6_16550A: ttyS1 at MMIO 0x2f8 (irq = 33, base_baud = 115200) is a 16550A
[    2.148370] of_serial 3e8.U6_16550A: ttyS2 at MMIO 0x3e8 (irq = 34, base_baud = 115200) is a 16550A
[    2.150699] of_serial 2e8.U6_16550A: ttyS3 at MMIO 0x2e8 (irq = 35, base_baud = 115200) is a 16550A
[    2.153017] [drm] Initialized drm 1.1.0 20060810
[    2.159540] brd: module loaded
[    2.162682] loop: module loaded
[    2.164001] tun: Universal TUN/TAP device driver, 1.6
[    2.165058] tun: (C) 1999-2004 Max Krasnyansky <maxk@qualcomm.com>
[    2.170165] PPP generic driver version 2.4.2
[    2.171439] PPP BSD Compression module registered
[    2.172482] PPP Deflate Compression module registered
[    2.173559] PPP MPPE Compression module registered
[    2.174560] NET: Registered protocol family 24
[    2.175500] pegasus: v0.9.3 (2013/04/25), Pegasus/Pegasus II USB Ethernet driver
[    2.177101] usbcore: registered new interface driver pegasus
[    2.178483] usbcore: registered new interface driver rtl8150
[    2.179711] usbcore: registered new interface driver r8152
[    2.180917] usbcore: registered new interface driver asix
[    2.182088] usbcore: registered new interface driver ax88179_178a
[    2.183414] usbcore: registered new interface driver cdc_ether
[    2.184684] usbcore: registered new interface driver dm9601
[    2.185909] usbcore: registered new interface driver CoreChips
[    2.187187] usbcore: registered new interface driver smsc75xx
[    2.188563] usbcore: registered new interface driver smsc95xx
[    2.189805] usbcore: registered new interface driver net1080
[    2.191022] usbcore: registered new interface driver plusb
[    2.192209] usbcore: registered new interface driver cdc_subset
[    2.193477] usbcore: registered new interface driver zaurus
[    2.194683] usbcore: registered new interface driver MOSCHIP usb-ethernet driver
[    2.196231] usbcore: registered new interface driver cdc_ncm
[    2.197555] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    2.198954] ehci-pci: EHCI PCI platform driver
[    2.199928] ehci-platform: EHCI generic platform driver
[    2.201067] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    2.202373] ohci-pci: OHCI PCI platform driver
[    2.203353] ohci-platform: OHCI generic platform driver
[    2.204797] usbcore: registered new interface driver cdc_acm
[    2.205969] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
[    2.207650] usbcore: registered new interface driver usb-storage
[    2.209338] mousedev: PS/2 mouse device common for all mice
[    2.211448] i2c /dev entries driver
[    2.212720] device-mapper: ioctl: 4.28.0-ioctl (2014-09-17) initialised: dm-devel@redhat.com
[    2.214829] sdhci: Secure Digital Host Controller Interface driver
[    2.216088] sdhci: Copyright(c) Pierre Ossman
[    2.217064] Synopsys Designware Multimedia Card Interface Driver
[    2.218839] sdhci-pltfm: SDHCI platform and OF driver helper
[    2.220146] ledtrig-cpu: registered to indicate activity on CPUs
[    2.221613] usbcore: registered new interface driver usbhid
[    2.222744] usbhid: USB HID core driver
[    2.223581] u32 classifier
[    2.224155]     Actions configured
[    2.224875] Netfilter messages via NETLINK v0.30.
[    2.225909] nf_conntrack version 0.5.0 (1819 buckets, 7276 max)
[    2.227686] ctnetlink v0.93: registering with nfnetlink.
[    2.229555] xt_time: kernel timezone is -0000
[    2.230810] ip_tables: (C) 2000-2006 Netfilter Core Team
[    2.231940] arp_tables: (C) 2002 David S. Miller
[    2.232986] TCP: cubic registered
[    2.233679] Initializing XFRM netlink socket
[    2.234723] NET: Registered protocol family 10
[    2.236605] mip6: Mobile IPv6
[    2.237378] ip6_tables: (C) 2000-2006 Netfilter Core Team
[    2.238897] NET: Registered protocol family 17
[    2.239889] NET: Registered protocol family 15
[    2.240949] 9pnet: Installing 9P2000 support
[    2.242670] Key type dns_resolver registered
[    2.244462] registered taskstats version 1
[    2.246379] Btrfs loaded
[    2.247448] otg_wakelock_init: No USB transceiver found
[    2.248613] drivers/rtc/hctosys.c: unable to open rtc device (rtc0)
[    2.267509] Sending DHCP requests ., OK
[    2.347398] IP-Config: Got DHCP answer from 192.168.33.1, my address is 192.168.33.15
[    2.349481] IP-Config: Complete:
[    2.350211]      device=eth0, hwaddr=02:15:15:15:15:15, ipaddr=192.168.33.15, mask=255.255.255.0, gw=192.168.33.1
[    2.352419]      host=192.168.33.15, domain=, nis-domain=(none)
[    2.353703]      bootserver=192.168.33.1, rootserver=0.0.0.0, rootpath=
[    2.355086]      nameserver0=155.210.3.12
[    2.356099] ALSA device list:
[    2.356764]   No soundcards found.
[    2.359430] VFS: Mounted root (9p filesystem) on device 0:15.
[    2.361219] devtmpfs: mounted
[    2.362040] Freeing unused kernel memory: 356K (ffffffc000b00000 - ffffffc000b59000)
Symbol kernel found in address 0x80b36000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b36000

Symbol kernel found in address 0x80b37000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b37000

Symbol kernel found in address 0x80b38000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b38000

Symbol kernel found in address 0x80b39000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b39000

Symbol kernel found in address 0x80b3a000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3a000

Symbol kernel found in address 0x80b3b000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3b000

Symbol kernel found in address 0x80b3c000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3c000

Symbol kernel found in address 0x80b3d000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3d000

Symbol kernel found in address 0x80b3e000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3e000

Symbol kernel found in address 0x80b3f000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b3f000

Symbol kernel found in address 0x80b40000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b40000

Symbol kernel found in address 0x80b41000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b41000

Symbol kernel found in address 0x80b42000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b42000

Symbol kernel found in address 0x80b43000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b43000

Symbol kernel found in address 0x80b44000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b44000

Symbol kernel found in address 0x80b45000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b45000

Symbol kernel found in address 0x80b46000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b46000

Symbol kernel found in address 0x80b47000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b47000

Symbol kernel found in address 0x80b48000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b48000

Symbol kernel found in address 0x80b49000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b49000

Symbol kernel found in address 0x80b4a000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4a000

Symbol kernel found in address 0x80b4b000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4b000

Symbol kernel found in address 0x80b4c000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4c000

Symbol kernel found in address 0x80b4d000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4d000

Symbol kernel found in address 0x80b4e000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4e000

Symbol kernel found in address 0x80b4f000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b4f000

Symbol kernel found in address 0x80b50000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b50000

Symbol kernel found in address 0x80b51000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b51000

Symbol kernel found in address 0x80b52000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b52000

Symbol kernel found in address 0x80b53000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b53000

Symbol kernel found in address 0x80b54000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b54000

Symbol kernel found in address 0x80b55000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b55000

Symbol kernel found in address 0x80b31000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b31000

Symbol kernel found in address 0x80b32000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b32000

Symbol kernel found in address 0x80b33000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b33000

Symbol kernel found in address 0x80b20000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b20000

Symbol kernel found in address 0x80b21000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b21000

Symbol kernel found in address 0x80b22000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b22000

Symbol kernel found in address 0x80b23000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b23000

Symbol kernel found in address 0x80b24000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b24000

Symbol kernel found in address 0x80b25000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b25000

Symbol kernel found in address 0x80b26000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b26000

Symbol kernel found in address 0x80b27000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b27000

Symbol kernel found in address 0x80b28000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b28000

Symbol kernel found in address 0x80b29000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b29000

Symbol kernel found in address 0x80b2a000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2a000

Symbol kernel found in address 0x80b2b000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2b000

Symbol kernel found in address 0x80b2c000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2c000

Symbol kernel found in address 0x80b2d000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2d000

Symbol kernel found in address 0x80b2e000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2e000

Symbol kernel found in address 0x80b2f000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b2f000

Symbol kernel found in address 0x80b00000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b00000

Symbol kernel found in address 0x80b01000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b01000

Symbol kernel found in address 0x80b02000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b02000

Symbol kernel found in address 0x80b03000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b03000

Symbol kernel found in address 0x80b04000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b04000

Symbol kernel found in address 0x80b05000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b05000

Symbol kernel found in address 0x80b06000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b06000

Symbol kernel found in address 0x80b07000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b07000

Symbol kernel found in address 0x80b08000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b08000

Symbol kernel found in address 0x80b09000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b09000

Symbol kernel found in address 0x80b0a000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0a000

Symbol kernel found in address 0x80b0b000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0b000

Symbol kernel found in address 0x80b0c000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0c000

Symbol kernel found in address 0x80b0d000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0d000

Symbol kernel found in address 0x80b0e000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0e000

Symbol kernel found in address 0x80b0f000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b0f000

Symbol kernel found in address 0x80b10000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b10000

Symbol kernel found in address 0x80b11000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b11000

Symbol kernel found in address 0x80b12000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b12000

Symbol kernel found in address 0x80b13000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b13000

Symbol kernel found in address 0x80b14000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b14000

Symbol kernel found in address 0x80b15000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b15000

Symbol kernel found in address 0x80b16000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b16000

Symbol kernel found in address 0x80b1d000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1d000

Symbol kernel found in address 0x80b1c000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1c000

Symbol kernel found in address 0x80b1b000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1b000

Symbol kernel found in address 0x80b1a000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1a000

Symbol kernel found in address 0x80b19000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b19000

Symbol kernel found in address 0x80b18000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b18000

Symbol kernel found in address 0x80b1e000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1e000

Symbol kernel found in address 0x80b1f000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b1f000

Mounting...
Symbol kernel found in address 0x80b56000
[|	|] Enter in backtrace extractor module...
Guest phy addr 0x80b56000

# 
[   12.616785] reboot: Restarting system

  # KVM session ended normally.
root@linaro-alip:~/DADE/scripts# e[Kexit

Script done on Tue 27 Feb 2018 07:52:58 PM UTC

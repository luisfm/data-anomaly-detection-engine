/*
 * Project name: DADE - Data Anomaly Detection Engine
 * Project managers: José Luis Briz and Darío Suárez 
 *					 Computer Architecture Group 
 *					 University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 28 feb 2018
 * File name: backtrace_ext.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <linux/list.h>
#include <string.h>
#include <linux/errno.h>
#include <openssl/md5.h>
#include <time.h>

#include "dade/backtrace_ext.h"
#include "dade/parser_stacktrace.h"

#define DEBUG		0

struct backtrace_container dock;
static FILE *fd;
int kernel_obj_num = 0;


/* debug purposes, print kernel list which contains addr and function name */
void debug__print_list(struct list_head *result_list) {

	struct backtrace *ptr;
	int i;

	i = 0;
	list_for_each_entry(ptr, result_list, list) {
		/* when assign [�btr_list = *result_list], the list head act as circular
		 * linked list.
		 */
		if (ptr->func_name == NULL || ptr->addr == NULL) {
			break;
		}

		printf("[%d] %s -> %s\n", i, ptr->addr, ptr->func_name);
		printf("\t  :\n");
		printf("\t  :\n");
		i = i + 1;
	}

	printf("\t  := Kernel object has been allocated.\n\n");

}

/* debug purposes, print dock structure */
void debug__print_dock(void) {

	printf("dock.btr.list:\n");
	debug__print_list(&dock.btr.list);

	printf("dock.obj_name:  \n\t%s\n", dock.obj_name);
	printf("dock.obj_value (ptr): \n\t%s\n", dock.obj_value);
	printf("dock.hash: \n\t%s\n", dock.hash);

}

/* procedure to save backtrace structure in a file */
void backtrace_ext__write_data(void) {
	
	struct backtrace *ptr;
	int i;
	
	fprintf(fd, "Kernel object No. %d:\n", kernel_obj_num);
	fprintf(fd, "\t obj_name: %s\n", dock.obj_name);
	fprintf(fd, "\t obj_value: %s\n", dock.obj_value);
	fprintf(fd, "\t hash: %s\n", dock.hash);

	i = 0;
	fprintf(fd, "\t stacktrace:\n");
	list_for_each_entry(ptr, &dock.btr.list, list) {
		if (ptr->func_name == NULL || ptr->addr == NULL) {
			break;
		}

		fprintf(fd, "\t [%d] {%s} \t %s\n", i, ptr->func_name, ptr->addr); 

		i = i + 1;
	}

	fprintf(fd, "-------------------------------------------------------------------\n\n");
	
	if (dock.obj_name != NULL) {
		free(dock.obj_name);
	}

	if (dock.obj_value != NULL) {
		free(dock.obj_value);
	}

}

/* function to generate signature with function addresses */
void backtrace_ext__generate_hash(void) {

	char *all_addr;
	struct backtrace *ptr;
	/* 16 (MD5_DIGEST_LENGTH) * 8b (char) = 128b */
	unsigned char digest[MD5_DIGEST_LENGTH];
	/* 128b / 4b = 32 hex char */
	char hash[MD5_DIGEST_LENGTH * 2 + 1];		
	int i;

	all_addr = malloc(sizeof(char) * BUFSIZE);
	list_for_each_entry(ptr, &dock.btr.list, list) {
		if (ptr->addr == NULL || ptr->func_name == NULL) {
			break;
		} 
		
		strncat(all_addr, ptr->addr, sizeof(char) * (ADDR_LEN + 2));
	}

    MD5((unsigned char*)all_addr, sizeof(char) * strlen(all_addr), 
													(unsigned char*)&digest);    
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
		/* 02x means if your provided value is less than two digits then 0 
		will be prepended. */
		sprintf(&hash[i * 2], "%02x", (unsigned int)digest[i]);
	}

	strncpy(dock.hash, hash, MD5_DIGEST_LENGTH * 2);

	if (all_addr != NULL) {
		free(all_addr);
	}

}


/* procedure to copy the list [from] to [dock] structure. In addition, extract
  obj_value */
void backtrace_ext__copy_obj_name(int list_length) {

	struct backtrace *ptr;
	size_t obj_name_len;
	int i;

	i = 0;
	list_for_each_entry(ptr, &dock.btr.list, list) {
		if (i == (list_length - 1)) {

			obj_name_len = strlen(ptr->func_name);
			dock.obj_name = malloc(sizeof(char) * (obj_name_len + 1));
			dock.obj_name[obj_name_len] = '\0';
			strncpy(dock.obj_name, ptr->func_name, obj_name_len);

			break;
		}

		i = i + 1;
	}

}

/* function to save stacktrace in backtrace extractor module */
int backtrace_ext__save_stacktrace(struct list_head *result_list, int list_length, char* obj_value, int obj_value_len) {

	kernel_obj_num = kernel_obj_num + 1;

	INIT_LIST_HEAD(&dock.btr.list);
	dock.hash[MD5_DIGEST_LENGTH * 2] = '\0';
	
	dock.obj_value = malloc(sizeof(char) * (obj_value_len - 1));
	dock.obj_value[obj_value_len - 2] = '\0';
	strncpy(dock.obj_value, obj_value, obj_value_len - 2);

	dock.btr.list = *result_list;
	backtrace_ext__copy_obj_name(list_length);
	backtrace_ext__generate_hash();

	backtrace_ext__write_data(); 

	return 0;
}

/* init procedure extractor */
int backtrace_ext__init(char *filename) {

	int ret;
	time_t local_time;
	char *local_time_str;

	fd = fopen("kobjects.data", "wb"); 

	local_time = time(NULL);
	local_time_str = ctime(&local_time);
	fprintf(fd, "Date %s\n", local_time_str);

	ret = parser_stacktrace__init(filename);
	if (ret < 0) {
		printf("Error in parser extractor module\n");
		fclose(fd);
		return -1;
	}

	return 0;
} 	

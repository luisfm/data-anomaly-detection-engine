/*
 * Project name: DADE - Data Anomaly Detection Engine 
 * Project managers: José Luis Briz and Darío Suárez
 *                   Computer Architecture Group
 *                   University of Zaragoza
 * Author: Luis Fueris Martín
 * Date: 30 march 2018
 * File name: parser_stacktrace.h
 */

#ifndef DADE__PARSER_STACKTRACE
#define DADE__PARSER_STACKTRACE

#include "dade/backtrace_ext.h"

#define EQUAL 			1
#define FUNCTION		2
#define ADDR			3

#define SKIP_N_LINES	11
	
int parser_stacktrace__init(char *filename);


#endif /* DADE__PARSER_STACKTACE */
